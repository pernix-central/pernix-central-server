# Pernix Certral Server #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###

This repository contains the Pernix-Central Back-End server project.

## How do I get set up? ###

### PostgresSQL ###
1. Get PostgresSQL, info can be [here](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart)
   
2. Create a new user [here](https://ubiq.co/database-blog/create-user-postgresql/)

### Ruby on Rails ###

1. Get RBENV (unless you already got it), info in [here](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-18-04-es).

2. Install the correct Ruby version (currently 3.1.2) -> `rbenv install 3.1.2`

3. Install the Bundler gem -> `gem install bundler`.

4. Install the other gems -> `bundle` OR `bundle install`.

5. Setup the database.yml file (There's a guide -> `databse.yml.example` .).
   1. Use this command to generate  the 'database.yml' file ->  
   `cp config/database.yml.example config/database.yml`.
   2. Check if the 'config/database.yml' file was successfully generated.

6. Setup the .env file for sending emails from development on your local machine (There's a guide -> `.env.example`).
   1. Use this command to generate  the '.env' file ->
   `cp .env.example .env`.
   2. Check if the '.env' file was successfully generated.

7.  Run `rake secret` as told in the .env file, to generate the `SECRET_KEY_BASE`.
- NOTE: ( In case you command you fail, use this command -> `bundle exec rake secret`)
   1. Something like this will be generated ->
   `74dfbcfab7b96fc9882120d260c4ba6f9a5a1729334aa5fa020e48bdc562eb0bba0f3f0a38a0bef8ea5e3760d3d69b5ae0ffbd19144eb204c0719411fa50de55` this is the `SECRET_KEY_BASE` .
   2. Now replace in the file '.env' previously created the 'SECRET_KEY_BASE' property, like this -> `SECRET_KEY_BASE=74dfbcfab7b96fc9882120d260c4ba6f9a5a1729334aa5fa020e48bdc562eb0bba0f3f0a38a0bef8ea5e3760d3d69b5ae0ffbd19144eb204c0719411fa50de55`  .

8.  Setup the database -> `rake db:setup`

### Run the local server ####

* Run the server -> `rails s`, this will start your local server at `localhost:3000/`

### Run tests: ###

1. `rake db:test:prepare`

2. `rspec`

3. Test coverage report can be found in `coverage/index.html`

#### Deploying to staging ####

Steps:

1. Run `cd pernix-central-server`
2. Run `git remote add heroku-staging https://git.heroku.com/pernix-central-api-staging.git`
3. Run `git checkout develop`
4. Run `git pull`
5. Run `bin/deploy heroku-staging develop:master`
6. Run `heroku run rake db:migrate -r heroku-staging`

### Contribution guidelines ###

* We use Rspec for writing tests, if unfamiliar with it, can find a guide [here](https://github.com/rspec/rspec-rails)

* Project uses HAML instead of the normal ERB/HTML based Rails templates, if unfamiliar with HAML you can find a guide [here](https://github.com/haml/haml)

* To handle JSON responses, this project uses the AMS gem, if unfamiliar with it, you can find a guide [here](https://github.com/rails-api/active_model_serializers)

* Before submitting pull requests for review, make sure to run `rubocop -a`, which will find code offenses, it will auto-fix some and point out the rest for you to fix. A guide of Rubocop use can be found [here](http://rubocop.readthedocs.io/en/latest/)

* To generate a visual guide of the database model run -> `erd`, a guide of the ERD gem can be found [here](https://github.com/voormedia/rails-erd)

### Where can I find the available endpoints?

* To find a description of the available endpoints, with their respective required payloads and produced responses, go to the Wiki pages [here](https://bitbucket.org/pernix-central/pernix-central-server/wiki/Home)

### Who do I talk to? ###

* Repo owner or admin

* Other community or team contact
