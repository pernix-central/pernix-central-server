# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.2"

# Freamework
gem "rails", "~> 6.1.7"

# Databases
gem "pg"

# Utilities
gem "active_model_serializers"
gem "aws-sdk"
gem "bcrypt"
gem "bootsnap", require: false
gem "bundle-audit"
gem "faker"
gem "fog"
gem "jwt"
gem "mimemagic", github: "mimemagicrb/mimemagic", ref: "01f92d86d15d85cfd0f20dabd025dcbd36a8a60f"
gem "ovirt-engine-sdk", "~> 4.3"
gem "puma"
gem "rack-cors"
gem "redis"
gem "sendgrid-ruby"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem "byebug"
  gem "dotenv-rails"
  gem "factory_bot_rails"
  gem "pry"
  gem "pry-byebug"
  gem "pry-remote"
  gem "pry-stack_explorer"
  gem "rails-controller-testing"
  gem "rspec-its"
  gem "rspec-rails"
  gem "rubocop"
  gem "rubocop-performance"
  gem "rubocop-rails"
  gem "rubocop-rspec"
  gem "shoulda-matchers"
end

group :development do
  gem "erb2haml"
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "rails-erd"
  gem "spring"
end

group :test do
  gem "database_cleaner"
  gem "rspec_junit_formatter"
  gem "simplecov", require: false
end
