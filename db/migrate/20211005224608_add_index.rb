# frozen_string_literal: true

# Add index to id_number and email in the employees table
class AddIndex < ActiveRecord::Migration[6.0]

  def change
    add_index :employees, :id_number, unique: true
    add_index :employees, :email, unique: true
  end

end
