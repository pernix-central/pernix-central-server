# frozen_string_literal: true

# Establishing notification employee polymorphic relationship that can be used for Vacation Requests and Wallet Requests
class EstablishNotificationEmployeePolymorphicRelationship < ActiveRecord::Migration[6.1]
  def up
    remove_foreign_key :notification_employees, column: :vacation_request_id
    rename_column :notification_employees, :vacation_request_id, :notifyable_id
    add_column :notification_employees, :notifyable_type, :string
    execute("UPDATE notification_employees  SET notifyable_type = 'VacationRequest' WHERE notifyable_id IS NOT NULL")
  end

  def down
    remove_column :notification_employees, :notifyable_type
    rename_column :notification_employees, :notifyable_id, :vacation_request_id
    add_foreign_key :notification_employees, :vacation_requests, column: :vacation_request_id
  end
end
