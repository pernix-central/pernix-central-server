# frozen_string_literal: true

# Add apprentice colum to awards table
class AddApprenticeColumn < ActiveRecord::Migration[6.1]
  def change
    add_column :awards, :apprentice, :boolean, default: false
  end

  def down
    remove_column :awards, :apprentice
  end
end
