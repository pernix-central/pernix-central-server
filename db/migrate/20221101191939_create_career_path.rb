# frozen_string_literal: true

# Create the career path table
class CreateCareerPath < ActiveRecord::Migration[6.0]
  def up
    create_table :career_paths do |t|
      t.string :information

      t.timestamps
    end
  end

  def down
    drop_table :career_paths
  end

end
