# frozen_string_literal: true

# Drop project associations tables
class DropProjectAssociations < ActiveRecord::Migration[6.0]
  def up
    drop_join_table :projects, :skills do |t|
      t.index :project_id
      t.index :skill_id
    end

    drop_join_table :clients, :projects do |t|
      t.index :client_id
      t.index :project_id
    end

    remove_reference :comments, :project
  end

  def down
    create_join_table :projects, :skills do |t|
      t.index :project_id
      t.index :skill_id
    end

    create_join_table :clients, :projects do |t|
      t.index :client_id
      t.index :project_id
    end

    add_reference :comments, :project
  end
end
