# frozen_string_literal: true

# Add image_url column to employees table
class AddImageUrlColumnToEmployees < ActiveRecord::Migration[5.2]
  def change
    add_column :employees, :image_url, :string, default: nil
  end
end
