# frozen_string_literal: true

# This migration creates the CreateMyWayPointsTransactions table.
class CreateMyWayPointsTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :my_way_points_transactions do |t|
      t.string :request_name, null: false
      t.integer :request_points, null: false
      t.string :request_type, default: "Credit", null: false
      t.string :request_comments, null: true
      t.string :status, default: "Pending", null: false
      t.bigint :reviewer_id, null: true
      t.datetime :review_date, null: true

      t.timestamps
    end
    add_reference :my_way_points_transactions, :employee, foreing_key: true
  end

  def down
    remove_reference :my_way_points_transactions, :employee, foreing_key: true
    drop_table :my_way_points_transactions
  end
end
