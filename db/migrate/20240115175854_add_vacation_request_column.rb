# frozen_string_literal: true

# Adding vacation request id column to notification employees table to easily retrieve vacation requests
# linked to notifications
class AddVacationRequestColumn < ActiveRecord::Migration[6.0]
  def up
    add_column :notification_employees, :vacation_request_id, :integer
    add_foreign_key :notification_employees, :vacation_requests, column: :vacation_request_id, primary_key: :id
  end

  def down
    remove_column :notification_employees, :vacation_request_id
  end
end
