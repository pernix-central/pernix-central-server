# frozen_string_literal: true

# Add sender to notification_employees table
class AddSenderToNotificationEmployees < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      change_table :notification_employees do |t|
        dir.up do
          t.column :sender_id, :integer
        end

        dir.down do
          t.remove :sender_id
        end
      end
    end
  end
end
