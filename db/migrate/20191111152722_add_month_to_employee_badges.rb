# frozen_string_literal: true

# Add month to employee_badges table
class AddMonthToEmployeeBadges < ActiveRecord::Migration[5.2]
  def change
    add_column :employee_badges, :month, :integer
  end
end
