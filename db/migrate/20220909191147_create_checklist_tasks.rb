# frozen_string_literal: true

# Create the checklist tasks table
class CreateChecklistTasks < ActiveRecord::Migration[6.0]
  def up
    create_table :checklist_tasks do |t|
      t.string :task_name
      t.string :task_type
      t.integer :employee_id

      t.timestamps
    end
  end
end
