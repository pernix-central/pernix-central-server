# frozen_string_literal: true

# Migration to add request removal notification settings
class AddRequestRemovalNotificationSetting < ActiveRecord::Migration[6.1]
  def change
    add_column :notification_settings, :request_removal, :json, default: { "email": true, "tray": true }
  end
end
