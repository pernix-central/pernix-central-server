# frozen_string_literal: true

# Initial database model
class InitialDatabaseModel < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.boolean :active, default: false
      t.string :email
      t.string :password_digest

      t.timestamps
    end

    create_table :employees do |t|
      t.boolean :has_car
      t.string :blog_url
      t.string :computer_serial_number
      t.string :mac_address
      t.date :hire_date
      t.boolean :on_vacation, default: false
      t.boolean :can_be_mentor, default: false
      t.string :role
      t.integer :total_score

      t.references :user, index: true

      t.timestamps
    end

    create_table :schedules do |t|
      t.decimal :total_work_hours

      t.references :employee

      t.timestamps
    end

    create_table :work_days do |t|
      t.string :week_day
      t.boolean :wfh, default: false

      t.references :schedule

      t.timestamps
    end

    create_table :periods do |t|
      t.time :start_time
      t.time :end_time

      t.references :work_day

      t.timestamps
    end

    create_table :teams do |t|
      t.string :name

      t.timestamps
    end

    create_table :projects do |t|
      t.string :name
      t.text :description
      t.string :status
      t.date :start_date
      t.date :end_date

      t.references :team

      t.timestamps
    end

    create_join_table :employees, :teams do |t|
      t.index :employee_id
      t.index :team_id
    end

    create_table :skills do |t|
      t.string :name
      t.string :type
      t.string :level

      t.timestamps
    end

    create_join_table :employees, :skills do |t|
      t.index :employee_id
      t.index :skill_id
    end

    create_join_table :projects, :skills do |t|
      t.index :project_id
      t.index :skill_id
    end

    create_table :clients do |t|
      t.string :name
      t.string :last_name
      t.string :contact_email
      t.string :company_name
      t.string :phone_number
      t.string :country

      t.timestamps
    end

    create_join_table :clients, :projects do |t|
      t.index :client_id
      t.index :project_id
    end

    create_table :activities do |t|
      t.string :name
      t.integer :points
      t.string :frequency

      t.timestamps
    end

    create_join_table :activities, :employees, table_name: :owners do |t|
      t.index :activity_id
      t.index :employee_id
    end

    create_table :participations do |t|
      t.date :date

      t.references :activity

      t.timestamps
    end

    create_join_table :employees, :participations do |t|
      t.index :employee_id
      t.index :participation_id
    end
  end
end
