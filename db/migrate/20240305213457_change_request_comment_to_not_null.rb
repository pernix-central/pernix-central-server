# frozen_string_literal: true

# Migration for change request_comment column to not_null
class ChangeRequestCommentToNotNull < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        change_column :my_way_points_transactions, :request_comments, :string, null: false, default: ""
      end
      dir.down do
        change_column :my_way_points_transactions, :request_comments, :string, null: true
      end
    end
  end
end
