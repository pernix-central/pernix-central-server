# frozen_string_literal: true

# Add enjoyed_days to employees table
class AddApprovedDaysToEmployees < ActiveRecord::Migration[6.0]

  def up
    add_column :employees, :approved_days, :integer, default: 0
  end

  def down
    remove_column :employees, :approved_days
  end

end
