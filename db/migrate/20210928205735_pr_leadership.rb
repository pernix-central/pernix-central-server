# frozen_string_literal: true

# Create pr_leaderships table
class PrLeadership < ActiveRecord::Migration[6.0]
  def up
    create_table :pr_leaderships do |t|
      t.string :name, null: false
      t.references :performance_review

      t.timestamps
    end
  end

end
