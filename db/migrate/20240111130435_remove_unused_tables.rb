# frozen_string_literal: true

# This migration remove all tables that are not being used and whose functionality was migrated to notion.
class RemoveUnusedTables < ActiveRecord::Migration[6.0]
  def up
    drop_table :benefit_employees
    drop_table :benefits
    drop_table :career_paths
    drop_table :evaluation_periods
    drop_table :performance_reviews
    drop_table :pr_achievements
    drop_table :pr_goals
    drop_table :pr_improvements
    drop_table :pr_leaderships
    drop_table :skills
    drop_table :tcp_answers
    drop_table :tcp_comment_expectations
    drop_table :tcp_competencies
    drop_table :tcp_plans
    drop_table :tcp_reviews
    drop_table :tcp_skills
  end

  def down
    create_table :benefit_employees, force: :cascade do |t|
      t.bigint "employee_id"
      t.bigint "benefit_id"
      t.integer "rating"
      t.index ["benefit_id"], name: "index_benefit_employees_on_benefit_id"
      t.index ["employee_id"], name: "index_benefit_employees_on_employee_id"

      t.timestamps
    end

    create_table "benefits", force: :cascade do |t|
      t.string "name"
      t.string "description"
      t.string "step"
      t.string "requirement"
      t.string "attachment_file_name"
      t.string "attachment_content_type"
      t.bigint "attachment_file_size"
      t.datetime "attachment_updated_at"
      t.timestamps
    end

    create_table "career_paths", force: :cascade do |t|
      t.string "information"

      t.timestamps
    end

    create_table "evaluation_periods", force: :cascade do |t|
      t.string "name"
      t.date "start_date"
      t.date "end_date"

      t.timestamps
    end

    create_table "performance_reviews", force: :cascade do |t|
      t.text "career_goal"
      t.bigint "raise"
      t.bigint "employee_id"
      t.bigint "evaluation_period_id"
      t.index ["employee_id"], name: "index_performance_reviews_on_employee_id"
      t.index ["evaluation_period_id"], name: "index_performance_reviews_on_evaluation_period_id"

      t.timestamps
    end

    create_table "pr_achievements", force: :cascade do |t|
      t.text "name"
      t.bigint "performance_review_id"
      t.index ["performance_review_id"], name: "index_pr_achievements_on_performance_review_id"

      t.timestamps
    end

    create_table "pr_goals", force: :cascade do |t|
      t.string "name"
      t.string "metric"
      t.bigint "reward"
      t.boolean "accomplished", default: false
      t.bigint "performance_review_id"
      t.index ["performance_review_id"], name: "index_pr_goals_on_performance_review_id"

      t.timestamps
    end

    create_table "pr_improvements", force: :cascade do |t|
      t.text "area"
      t.text "plan"
      t.bigint "performance_review_id"
      t.index ["performance_review_id"], name: "index_pr_improvements_on_performance_review_id"

      t.timestamps
    end

    create_table "pr_leaderships", force: :cascade do |t|
      t.string "name", null: false
      t.bigint "performance_review_id"
      t.datetime "created_at", precision: 6, null: false
      t.datetime "updated_at", precision: 6, null: false
      t.index ["performance_review_id"], name: "index_pr_leaderships_on_performance_review_id"
    end

    create_table "skills", force: :cascade do |t|
      t.string "name"
      t.string "skill_type"
      t.string "level"

      t.timestamps
    end

    create_table "tcp_answers", force: :cascade do |t|
      t.string "competency"
      t.string "current_position"
      t.bigint "tcp_review_id"
      t.index ["tcp_review_id"], name: "index_tcp_answers_on_tcp_review_id"

      t.timestamps
    end

    create_table "tcp_comment_expectations", force: :cascade do |t|
      t.text "text"
      t.bigint "tcp_answer_id"
      t.index ["tcp_answer_id"], name: "index_tcp_comment_expectations_on_tcp_answer_id"

      t.timestamps
    end

    create_table "tcp_competencies", force: :cascade do |t|
      t.string "text"

      t.timestamps
    end

    create_table "tcp_plans", force: :cascade do |t|
      t.text "text"
      t.bigint "tcp_answer_id"
      t.index ["tcp_answer_id"], name: "index_tcp_plans_on_tcp_answer_id"

      t.timestamps
    end

    create_table "tcp_reviews", force: :cascade do |t|
      t.bigint "evaluation_period_id"
      t.bigint "employee_id"
      t.index ["employee_id"], name: "index_tcp_reviews_on_employee_id"
      t.index ["evaluation_period_id"], name: "index_tcp_reviews_on_evaluation_period_id"

      t.timestamps
    end

    create_table "tcp_skills", force: :cascade do |t|
      t.bigint "employee_id"
      t.bigint "skill_id"
      t.string "level"
      t.index ["employee_id"], name: "index_tcp_skills_on_employee_id"
      t.index ["skill_id"], name: "index_tcp_skills_on_skill_id"

      t.timestamps
    end
  end
end
