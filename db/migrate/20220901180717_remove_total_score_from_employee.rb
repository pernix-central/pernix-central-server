# frozen_string_literal: true

# Remove total_score from employee table
class RemoveTotalScoreFromEmployee < ActiveRecord::Migration[6.0]
  def change
    remove_column :employees, :total_score, :integer
  end
end
