# frozen_string_literal: true

# Create images table and references to employee table
class AddImagesToEmployees < ActiveRecord::Migration[5.2]
  def up
    create_table :images do |t|
      t.references :employee, index: true
      t.timestamps
    end

    add_attachment :images, :attachment

    remove_column :employees, :image_url
  end

  def down
    remove_attachment :images, :attachment

    drop_table :images
  end
end
