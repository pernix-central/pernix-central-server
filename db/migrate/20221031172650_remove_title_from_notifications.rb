# frozen_string_literal: true

# Remove title from notifications table
class RemoveTitleFromNotifications < ActiveRecord::Migration[6.0]
  def change
    remove_column :notifications, :title, :string
  end
end
