# frozen_string_literal: true

# Create the notification_employees table
class CreateNotificationEmployees < ActiveRecord::Migration[6.0]
  def up
    create_table :notification_employees do |t|
      t.references :employee
      t.references :notification

      t.boolean :read, default: false

      t.timestamps
    end
  end

  def down
    drop_table :notification_employees
  end
end
