# frozen_string_literal: true

# Add employee reference to suggestions table
class AddEmployeeToSuggestions < ActiveRecord::Migration[6.0]
  def up
    add_reference :suggestions, :employee
  end

  def down
    remove_reference :suggestions, :employee
  end
end
