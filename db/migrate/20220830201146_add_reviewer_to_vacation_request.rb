# frozen_string_literal: true

# Add reviewer to vacation_request table
class AddReviewerToVacationRequest < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      change_table :vacation_requests do |t|
        dir.up do
          t.remove :employee_name, :reviewer
          t.column :reviewer_id, :integer
        end

        dir.down do
          t.remove :reviewer_id
        end
      end
    end
  end
end
