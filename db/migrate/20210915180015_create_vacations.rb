# frozen_string_literal: true

# Create the vacations table
class CreateVacations < ActiveRecord::Migration[6.0]

  def up
    create_table :vacations do |t|
      t.datetime :date, null: false
      t.timestamps
    end

    add_reference :vacations, :vacation_request, foreing_key: true
  end

  def down
    remove_reference :vacations, :vacation_request, foreing_key: true
    drop_table :vacations
  end

end
