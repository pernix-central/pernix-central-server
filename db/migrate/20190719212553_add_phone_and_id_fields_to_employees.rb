# frozen_string_literal: true

# Add phone_number and id_number to employees table
class AddPhoneAndIdFieldsToEmployees < ActiveRecord::Migration[5.2]
  def change
    change_table :employees, bulk: true do |t|
      t.column :phone_number, :string
      t.column :id_number, :string
    end
  end
end
