# frozen_string_literal: true

# Creation the PerformanceReview module
class AddPerformanceReviewModule < ActiveRecord::Migration[5.2]

  def change
    create_table :evaluation_periods do |t|
      t.string :name

      t.timestamps
    end

    create_table :performance_reviews do |t|
      t.text :career_goal
      t.integer :raise

      t.references :employee
      t.references :evaluation_period

      t.timestamps
    end

    create_table :pr_achievements do |t|
      t.text :name

      t.references :performance_review

      t.timestamps
    end

    create_table :pr_improvements do |t|
      t.text :area
      t.text :plan

      t.references :performance_review

      t.timestamps
    end

    create_table :pr_goals do |t|
      t.string :name
      t.string :metric
      t.integer :reward
      t.boolean :accomplished, default: false

      t.references :performance_review

      t.timestamps
    end
  end

end
