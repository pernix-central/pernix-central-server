# frozen_string_literal: true

# Drop project table
class DropTableProjects < ActiveRecord::Migration[6.0]
  def up
    drop_table :projects
  end

  def down
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.string :status
      t.date :start_date
      t.date :end_date

      t.references :team

      t.timestamps
    end
  end
end
