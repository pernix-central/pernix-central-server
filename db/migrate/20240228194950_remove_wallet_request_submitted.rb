# frozen_string_literal: true

# Migration to remove wallet_request_submitted
class RemoveWalletRequestSubmitted < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      change_table :notification_settings do |t|
        dir.up do
          t.remove :wallet_request_submitted
        end

        dir.down do
          t.json :wallet_request_submitted, default: { "email": true, "tray": true }
        end
      end
    end
  end
end
