# frozen_string_literal: true

# Add default vale to employees column
class AddDefaultValeToEmployeesColumn < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:employees, :total_score, from: nil, to: 0)
  end
end
