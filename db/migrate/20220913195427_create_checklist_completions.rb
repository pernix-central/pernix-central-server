# frozen_string_literal: true

# Create the checklist completions table
class CreateChecklistCompletions < ActiveRecord::Migration[6.0]
  def up
    create_table :checklist_completions do |t|
      t.integer :employee_id
      t.integer :task_id

      t.timestamps
    end
  end
end
