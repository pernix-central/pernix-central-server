# frozen_string_literal: true

# Change integer columns to big integers
class ChangeIntegerColumnsToBigInts < ActiveRecord::Migration[6.0]
  def up
    change_column :activities, :points, :integer, limit: 8
    change_column :badges, :point_value, :integer, limit: 8
    change_table :employee_badges, bulk: true do |t|
      t.change :qty, :integer, limit: 8
      t.change :month, :integer, limit: 8
    end
    change_column :employees, :total_score, :integer, limit: 8
    change_column :performance_reviews, :raise, :integer, limit: 8
    change_column :pr_goals, :reward, :integer, limit: 8
    change_column :roles, :tier, :integer, limit: 8
  end

  def down
    change_column :activities, :points, :integer, limit: 4
    change_column :badges, :point_value, :integer, limit: 4
    change_table :employee_badges, bulk: true do |t|
      t.change :qty, :integer, limit: 4
      t.change :month, :integer, limit: 4
    end
    change_column :employees, :total_score, :integer, limit: 4
    change_column :performance_reviews, :raise, :integer, limit: 4
    change_column :pr_goals, :reward, :integer, limit: 4
    change_column :roles, :tier, :integer, limit: 4
  end
end
