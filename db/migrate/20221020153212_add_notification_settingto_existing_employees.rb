# frozen_string_literal: true

# Add notification_setting to existing employees
class AddNotificationSettingtoExistingEmployees < ActiveRecord::Migration[6.0]
  def change
    Employee.all.find_each do |e|
      NotificationSetting.create(employee: e) if NotificationSetting.find_by(employee: e).nil?
    end
  end
end
