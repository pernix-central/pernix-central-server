# frozen_string_literal: true

# Remove approved_days to employees table
class RemoveApprovedDaysColumn < ActiveRecord::Migration[6.1]
  def up
    remove_column :employees, :approved_days
  end

  def down
    add_column :employees, :approved_days, :integer, default: 0
  end
end
