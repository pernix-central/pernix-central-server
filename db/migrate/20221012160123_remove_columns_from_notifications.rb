# frozen_string_literal: true

# Remove attachments and date columns from notifications table
class RemoveColumnsFromNotifications < ActiveRecord::Migration[6.0]
  def up
    remove_attachment :notifications, :attachment
    remove_column :notifications, :date, :date
  end

  def down
    add_attachment :notifications, :attachment
    add_column :notifications, :date, :date
  end
end
