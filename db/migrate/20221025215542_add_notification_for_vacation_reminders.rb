# frozen_string_literal: true

# Add vacation request reminders to notification settings
class AddNotificationForVacationReminders < ActiveRecord::Migration[6.0]
  def up
    add_column :notification_settings, :vacation_request_reminder, :json, default: { "email": true, "tray": true }
  end

  def down
    remove_column :notification_settings, :vacation_request_reminder
  end
end
