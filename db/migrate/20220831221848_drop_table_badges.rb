# frozen_string_literal: true

# Drop badge table and employee_badges association table
class DropTableBadges < ActiveRecord::Migration[6.0]
  def up
    drop_table :badges
    drop_table :employee_badges
  end

  def down
    create_table :badges, bulk: true do |t|
      t.string :name
      t.integer :point_value, default: 0
      t.text :description
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.bigint :attachment_file_size
      t.datetime :attachment_updated_at

      t.timestamps
    end

    create_table :employee_badges do |t|
      t.references :badge
      t.references :employee
      t.integer :qty, default: 1
      t.integer :month

      t.timestamps
    end
  end
end
