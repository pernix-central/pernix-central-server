# frozen_string_literal: true

# Drop activities table and participants table
class DropTableActivities < ActiveRecord::Migration[6.0]
  def up
    drop_table :activities
    drop_table :participants
  end

  def down
    create_table :activities do |t|
      t.string :name
      t.integer :points
      t.string :frequency

      t.timestamps
    end

    create_table :participants do |t|
      t.date :date
      t.boolean :is_owner

      t.references :activities
      t.references :employee

      t.timestamps
    end
  end
end
