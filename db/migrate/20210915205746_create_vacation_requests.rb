# frozen_string_literal: true

# Create the vacation_requests table
class CreateVacationRequests < ActiveRecord::Migration[6.0]

  def change
    create_table :vacation_requests do |t|
      t.datetime :requested_date, null: false
      t.string :client_approval
      t.string :status, default: "Pending", null: false
      t.string :reviewer, null: true
      t.string :employee_name, null: false
      t.datetime :review_date, null: true
      t.timestamps
    end
    add_reference :vacation_requests, :employee, foreing_key: true
  end

  def down
    remove_reference :vacation_requests, :employee, foreing_key: true
    drop_table :vacation_requests
  end

end
