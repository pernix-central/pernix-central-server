# frozen_string_literal: true

# Create the benefits table
class CreateBenefits < ActiveRecord::Migration[6.0]
  def up
    create_table :benefits do |t|
      t.string :name
      t.string :description
      t.string :step
      t.string :requirement
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.bigint :attachment_file_size
      t.datetime :attachment_updated_at

      t.timestamps
    end
  end
end
