# frozen_string_literal: true

# This migration adds the reset_token and reset_token_created_at columns.
class AddResetTokenToEmployees < ActiveRecord::Migration[6.0]
  def change
    change_table :employees, bulk: true do |t|
      t.string :reset_token
      t.datetime :reset_token_created_at
      t.index :reset_token, unique: true
    end
  end
end
