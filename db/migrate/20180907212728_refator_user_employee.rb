# frozen_string_literal: true

# Refactor to employees creation
class RefatorUserEmployee < ActiveRecord::Migration[5.2]

  # User relationship
  class User < ApplicationRecord
    has_one :employee
  end

  def change
    change_table :employees, bulk: true do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.boolean :active, default: false
      t.string :email
      t.string :password_digest
    end

    User.all.find_each do |u|
      u.employee.update(
        first_name: u.first_name,
        last_name: u.last_name,
        birthdate: u.birthdate,
        active: u.active,
        email: u.email,
        password_digest: u.password_digest,
      )
    end

    remove_reference :employees, :user

    drop_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.boolean :active, default: false
      t.string :email
      t.string :password_digest

      t.timestamps
    end
  end
end
