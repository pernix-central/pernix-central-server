# frozen_string_literal: true

# Add vacation_control column to employees
class AddVacationControlToEmployeeTable < ActiveRecord::Migration[6.0]
  def up
    add_column :employees, :vacation_control, :boolean, default: true
  end

  def down
    remove_column :employees, :vacation_control
  end
end
