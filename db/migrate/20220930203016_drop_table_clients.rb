# frozen_string_literal: true

# Drop client table
class DropTableClients < ActiveRecord::Migration[6.0]
  def up
    drop_table :clients
  end

  def down
    create_table :clients do |t|
      t.string :name
      t.string :last_name
      t.string :contact_email
      t.string :company_name
      t.string :phone_number
      t.string :country

      t.timestamps
    end
  end
end
