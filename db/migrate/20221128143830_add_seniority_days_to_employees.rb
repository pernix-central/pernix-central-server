# frozen_string_literal: true

# Update seniority_days of employees
class AddSeniorityDaysToEmployees < ActiveRecord::Migration[6.0]
  def up
    Employee.all.find_each do |employee|
      employee.update(seniority_days: employee.employee_seniority_days)
    end
  end
end
