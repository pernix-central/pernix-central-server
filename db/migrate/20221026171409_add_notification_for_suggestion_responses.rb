# frozen_string_literal: true

# Add suggestion_responses to notification_settings
class AddNotificationForSuggestionResponses < ActiveRecord::Migration[6.0]
  def up
    add_column :notification_settings, :suggestion_responses, :json, default: { "email": true, "tray": true }
  end

  def down
    remove_column :notification_settings, :suggestion_responses
  end
end
