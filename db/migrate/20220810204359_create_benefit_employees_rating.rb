# frozen_string_literal: true

# Create the benefit_employees table
class CreateBenefitEmployeesRating < ActiveRecord::Migration[6.0]
  def up
    create_table :benefit_employees do |t|
      t.references :employee
      t.references :benefit

      t.integer :rating

      t.timestamps
    end
  end
end
