# frozen_string_literal: true

# Add change_schedule column to notification_settings
class AddScheduleColumnToNotificationSettings < ActiveRecord::Migration[6.0]
  def up
    add_column :notification_settings, :change_schedule, :json, default: { "email": true, "tray": true }
  end

  def down
    remove_column :notification_settings, :change_schedule
  end
end
