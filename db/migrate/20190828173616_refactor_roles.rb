# frozen_string_literal: true

# Refactor to roles creation
class RefactorRoles < ActiveRecord::Migration[5.2]
  def up
    create_table :roles, bulk: true do |t|
      t.string :name
      t.integer :tier

      t.timestamps
    end

    add_reference :employees, :role, index: true

    roles = [
      { name: "Admin", tier: 0 },
      { name: "Crafter", tier: 1 },
      { name: "Apprentice", tier: 2 },
    ]

    roles.each do |r|
      Role.create(r)
    end

    Employee.all.find_each do |e|
      current_role = e.role.try(:capitalize)

      r = Role.find_by(name: current_role)

      e.update(role_id: r.id) if r
    end

    remove_column :employees, :role
  end

  def down
    add_column :employees, :role, :string

    Employee.all.find_each do |e|
      r = e.role
      e.update(role: r.name.downcase) if r
    end

    remove_reference :employees, :role, index: true
    drop_table :roles
  end
end
