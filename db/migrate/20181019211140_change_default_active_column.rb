# frozen_string_literal: true

# Change default active column in employees table
class ChangeDefaultActiveColumn < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:employees, :active, from: false, to: true)
  end
end
