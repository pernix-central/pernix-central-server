# frozen_string_literal: true

# Create the notification_settings table
class CreateNotificationSettings < ActiveRecord::Migration[6.0]
  def up
    create_table :notification_settings do |t|
      t.references :employee

      t.json :anniversary, default: { "email": true, "tray": true }
      t.json :birthday, default: { "email": true, "tray": true }
      t.json :change_role, default: { "email": true, "tray": true }
      t.json :vacation_request_submitted, default: { "email": true, "tray": true }
      t.json :vacation_request_reviewed, default: { "email": true, "tray": true }

      t.timestamps
    end
  end

  def down
    drop_table :notification_settings
  end
end
