# frozen_string_literal: true

# Create suggestions table
class AddSuggestionsBox < ActiveRecord::Migration[5.2]
  def change
    create_table :suggestions, bulk: true, limit: 8 do |t|
      t.string :title
      t.text :description
      t.string :author
      t.boolean :management_resolved, default: false

      t.timestamps
    end

    add_reference :comments, :suggestion, index: true
    add_column :comments, :anonymus, :boolean, default: false
  end
end
