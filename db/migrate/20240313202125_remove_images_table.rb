# frozen_string_literal: true

# Remove the Images table
class RemoveImagesTable < ActiveRecord::Migration[6.1]
  def up
    drop_table :images
  end

  def down
    create_table :images do |t|
      t.references :employee, index: true
      t.timestamps
    end

    add_attachment :images, :attachment
  end
end
