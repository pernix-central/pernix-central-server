# frozen_string_literal: true

# Create comments table and references to team, project and employee
class AddCommentsToTeamsAndProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :text

      t.references :team
      t.references :project
      t.references :employee

      t.timestamps
    end
  end
end
