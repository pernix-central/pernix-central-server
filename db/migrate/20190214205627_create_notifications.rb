# frozen_string_literal: true

# Create the notifications table
class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.date :date
      t.string :notification_type
      t.string :title
      t.text :content
      t.boolean :active, default: false

      t.timestamps
    end

    add_attachment :notifications, :attachment
  end
end
