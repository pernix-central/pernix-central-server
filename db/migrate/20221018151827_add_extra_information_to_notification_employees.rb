# frozen_string_literal: true

# Add extra_information column to notification_employees table
class AddExtraInformationToNotificationEmployees < ActiveRecord::Migration[6.0]
  def up
    add_column :notification_employees, :extra_information, :string
  end

  def down
    remove_column :notification_employees, :extra_information
  end
end
