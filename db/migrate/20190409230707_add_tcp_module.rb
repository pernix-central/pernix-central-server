# frozen_string_literal: true

# Creation the TCP module
class AddTcpModule < ActiveRecord::Migration[5.2]

  def change
    change_table :evaluation_periods, bulk: true do |t|
      t.date :start_date
      t.date :end_date
    end

    create_table :tcp_reviews do |t|
      t.references :evaluation_period
      t.references :employee

      t.timestamps
    end

    create_table :tcp_competencies do |t|
      t.string :text

      t.timestamps
    end

    create_table :tcp_answers do |t|
      t.string :competency
      t.string :current_position

      t.references :tcp_review

      t.timestamps
    end

    create_table :tcp_comment_expectations do |t|
      t.text :text

      t.references :tcp_answer

      t.timestamps
    end

    create_table :tcp_plans do |t|
      t.text :text

      t.references :tcp_answer

      t.timestamps
    end
  end

end
