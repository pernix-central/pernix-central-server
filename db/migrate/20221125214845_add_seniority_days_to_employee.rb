# frozen_string_literal: true

# Add seniority_days to employee table
class AddSeniorityDaysToEmployee < ActiveRecord::Migration[6.0]
  def up
    add_column :employees, :seniority_days, :integer, default: 0
  end

  def down
    remove_column :employees, :seniority_days
  end

end
