# frozen_string_literal: true

# This migration creates the CreateAwards table.
class CreateAwards < ActiveRecord::Migration[6.0]
  def change
    create_table :awards do |t|
      t.string :name, null: false
      t.integer :points, null: false

      t.timestamps
    end
  end

  def down
    drop_table :awards
  end
end
