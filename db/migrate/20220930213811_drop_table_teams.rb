# frozen_string_literal: true

# Drop team table and associations
class DropTableTeams < ActiveRecord::Migration[6.0]
  def up
    remove_reference :comments, :team

    drop_join_table :employees, :teams do |t|
      t.index :employee_id
      t.index :team_id
    end

    drop_table :teams
  end

  def down
    create_table :teams do |t|
      t.string :name

      t.timestamps
    end

    create_join_table :employees, :teams do |t|
      t.index :employee_id
      t.index :team_id
    end

    add_reference :comments, :team
  end
end
