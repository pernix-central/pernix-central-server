# frozen_string_literal: true

# Create the official_links table
class CreateOfficialLinks < ActiveRecord::Migration[6.0]

  def change
    create_table :official_links do |t|
      t.string :name, null: false
      t.string :hyperlink, null: false

      t.timestamps
    end
  end

  def down
    drop_table :official_links
  end
end
