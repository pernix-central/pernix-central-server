# frozen_string_literal: true

# Migration to add notification settings for requests related to the Pernix Wallet
class AddWalletRequestNotificationSettingsColumn < ActiveRecord::Migration[6.1]
  def up
    change_table :notification_settings, bulk: true do |t|
      t.json :wallet_request_submitted, default: { "email": true, "tray": true }
      t.json :wallet_request_reviewed, default: { "email": true, "tray": true }
      t.json :wallet_request_reminder, default: { "email": true, "tray": true }
    end
  end

  def down
    change_table :notification_settings, bulk: true do |t|
      t.remove :wallet_request_submitted
      t.remove :wallet_request_reviewed
      t.remove :wallet_request_reminder
    end
  end
end
