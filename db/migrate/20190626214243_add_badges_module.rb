# frozen_string_literal: true

# Creation the Badges module
class AddBadgesModule < ActiveRecord::Migration[5.2]

  def change
    create_table :badges, bulk: true do |t|
      t.string :name
      t.integer :point_value, default: 0

      t.timestamps
    end

    add_attachment :badges, :attachment

    create_table :employee_badges do |t|
      t.references :badge
      t.references :employee

      t.integer :qty, default: 1

      t.timestamps
    end
  end

end
