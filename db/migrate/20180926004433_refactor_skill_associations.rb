# frozen_string_literal: true

# Refactor skill associations
class RefactorSkillAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :tcp_skills do |t|
      t.references :employee
      t.references :skill
      t.string :level

      t.timestamps
    end

    drop_join_table :employees, :skills do |t|
      t.index :employee_id
      t.index :skill_id
    end
  end
end
