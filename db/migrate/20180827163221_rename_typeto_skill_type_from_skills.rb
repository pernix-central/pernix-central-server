# frozen_string_literal: true

# Rename type to skill_type from skills table
class RenameTypetoSkillTypeFromSkills < ActiveRecord::Migration[5.2]
  def change
    rename_column :skills, :type, :skill_type
  end
end
