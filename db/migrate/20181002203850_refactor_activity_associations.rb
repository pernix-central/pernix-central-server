# frozen_string_literal: true

# Refactor to activity associations
class RefactorActivityAssociations < ActiveRecord::Migration[5.2]
  def change
    drop_join_table :activities, :employees, table_name: :owners do |t|
      t.index :activity_id
      t.index :employee_id
    end

    rename_table :participations, :participants
    add_column :participants, :is_owner, :boolean, defualt: false
    add_reference :participants, :employee, index: true

    drop_join_table :employees, :participations do |t|
      t.index :employee_id
      t.index :participation_id
    end
  end
end
