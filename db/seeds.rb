# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

admin_role = Role.create(name: "Admin", tier: 0)
crafter_role = Role.create(name: "Crafter", tier: 1)
apprentice_role = Role.create(name: "Apprentice", tier: 2)

# Employees seeds

e0 = FactoryBot.build(:employee)
e0.email = "admin@pernix.com"
e0.role = admin_role
e0.save!

e1 = FactoryBot.create(:employee)
e1.email = "crafter@pernix.com"
e1.role = crafter_role
e1.vacation_control = true
e1.save!

e3 = FactoryBot.create(:employee)
e3.email = "apprentice@pernix.com"
e3.role = apprentice_role
e3.save!

10.times do
  FactoryBot.create(:employee, role: crafter_role, vacation_control: true)
end

# Vacation request and performance review seeds

Employee.all.map.each do |employee|
  $countVR = 0
  $countV = 0
  $randVR = rand 0..2
  $randV = rand 1..4

  until $randVR < $countVR
    $countVR += 1

    # Create random vacation request

    vacation_request = VacationRequest.create(requested_date: DateTime.now, client_approval: "XXXX",
                                              status: %w[accepted pending].sample, employee_id: employee.id)

    until $randV < $countV
      $countV += 1
      Vacation.create(date: Faker::Date.between(from: 0.days.ago, to: 20.days.from_now),
                      vacation_request_id: vacation_request.id)
    end

    if vacation_request.vacations.present?
      if vacation_request.accepted?
        employee.update!(on_vacation: vacation_request.vacations.find do |vac|
                                        vac.date.to_date == Time.zone.today
                                      end.present?)
      else
        employee.update!(on_vacation: false)
      end
    else
      vacation_request.destroy
    end
  end
end

# Create wallet awards
award_values = [
  ["Presencia en la oficina vive cerca", 5],
  ["Presencia en la oficina vive largo", 10],
  ["Presentar en el Town Hall", 5],
  ["Atender el Town hall", 1],
  ["Cámara encendida en el Town Hall", 1],
  ["Cumpleaños", 10],
  ["Participar en actividades empresariales", 15],
  ["Dirigir un Town Hall", 15],
  ["Buena calificación como mentor", 10],
  ["Dar un reconocimiento", 2],
  ["Recibir un reconocimiento", 2],
  ["Hacer una tarea extraordinaria", 2],
  ["Dirigir un Open Space", 2],
  ["Hacer una presentación técnica abierta a todo el equipo", 15],
]

award_values.each do |award_value|
  Award.create(name: award_value[0], points: award_value[1])
end

# Create notifications
NOTIFICATIONS = Notification.notification_types.keys.map(&:to_sym).freeze
NOTIFICATIONS.each do |notification_type|
  FactoryBot.create(:notification, notification_type)
end
