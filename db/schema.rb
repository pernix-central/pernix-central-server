# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_04_09_134422) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "awards", force: :cascade do |t|
    t.string "name", null: false
    t.integer "points", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "apprentice", default: false
  end

  create_table "checklist_completions", force: :cascade do |t|
    t.integer "employee_id"
    t.integer "task_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "checklist_tasks", force: :cascade do |t|
    t.string "task_name"
    t.string "task_type"
    t.integer "employee_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text "text"
    t.bigint "employee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "suggestion_id"
    t.boolean "anonymus", default: false
    t.index ["employee_id"], name: "index_comments_on_employee_id"
    t.index ["suggestion_id"], name: "index_comments_on_suggestion_id"
  end

  create_table "employees", force: :cascade do |t|
    t.boolean "has_car"
    t.string "blog_url"
    t.string "computer_serial_number"
    t.string "mac_address"
    t.date "hire_date"
    t.boolean "on_vacation", default: false
    t.boolean "can_be_mentor", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.date "birthdate"
    t.boolean "active", default: true
    t.string "email"
    t.string "password_digest"
    t.string "phone_number"
    t.string "id_number"
    t.bigint "role_id"
    t.integer "seniority_days", default: 0
    t.boolean "vacation_control", default: true
    t.string "reset_token"
    t.datetime "reset_token_created_at"
    t.index ["email"], name: "index_employees_on_email", unique: true
    t.index ["id_number"], name: "index_employees_on_id_number", unique: true
    t.index ["reset_token"], name: "index_employees_on_reset_token", unique: true
    t.index ["role_id"], name: "index_employees_on_role_id"
  end

  create_table "my_way_points_transactions", force: :cascade do |t|
    t.string "request_name", null: false
    t.integer "request_points", null: false
    t.string "request_type", default: "Credit", null: false
    t.string "request_comments", default: "", null: false
    t.string "status", default: "Pending", null: false
    t.bigint "reviewer_id"
    t.datetime "review_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "employee_id"
    t.index ["employee_id"], name: "index_my_way_points_transactions_on_employee_id"
  end

  create_table "notification_employees", force: :cascade do |t|
    t.bigint "employee_id"
    t.bigint "notification_id"
    t.boolean "read", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "sender_id"
    t.string "extra_information"
    t.integer "notifyable_id"
    t.string "notifyable_type"
    t.index ["employee_id"], name: "index_notification_employees_on_employee_id"
    t.index ["notification_id"], name: "index_notification_employees_on_notification_id"
  end

  create_table "notification_settings", force: :cascade do |t|
    t.bigint "employee_id"
    t.json "anniversary", default: {"email"=>true, "tray"=>true}
    t.json "birthday", default: {"email"=>true, "tray"=>true}
    t.json "change_role", default: {"email"=>true, "tray"=>true}
    t.json "vacation_request_submitted", default: {"email"=>true, "tray"=>true}
    t.json "vacation_request_reviewed", default: {"email"=>true, "tray"=>true}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "suggestion_responses", default: {"email"=>true, "tray"=>true}
    t.json "vacation_request_reminder", default: {"email"=>true, "tray"=>true}
    t.json "change_schedule", default: {"email"=>true, "tray"=>true}
    t.json "wallet_request_reviewed", default: {"email"=>true, "tray"=>true}
    t.json "wallet_request_reminder", default: {"email"=>true, "tray"=>true}
    t.json "request_removal", default: {"email"=>true, "tray"=>true}
    t.index ["employee_id"], name: "index_notification_settings_on_employee_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "notification_type"
    t.text "content"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "official_links", force: :cascade do |t|
    t.string "name", null: false
    t.string "hyperlink", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "periods", force: :cascade do |t|
    t.time "start_time"
    t.time "end_time"
    t.bigint "work_day_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["work_day_id"], name: "index_periods_on_work_day_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.bigint "tier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.decimal "total_work_hours"
    t.bigint "employee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id"], name: "index_schedules_on_employee_id"
  end

  create_table "suggestions", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "author"
    t.boolean "management_resolved", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "employee_id"
    t.index ["employee_id"], name: "index_suggestions_on_employee_id"
  end

  create_table "vacation_requests", force: :cascade do |t|
    t.datetime "requested_date", null: false
    t.string "client_approval"
    t.string "status", default: "Pending", null: false
    t.datetime "review_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "employee_id", null: false
    t.integer "reviewer_id"
    t.index ["employee_id"], name: "index_vacation_requests_on_employee_id"
  end

  create_table "vacations", force: :cascade do |t|
    t.datetime "date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "vacation_request_id", null: false
    t.index ["vacation_request_id"], name: "index_vacations_on_vacation_request_id"
  end

  create_table "work_days", force: :cascade do |t|
    t.string "week_day"
    t.boolean "wfh", default: false
    t.bigint "schedule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["schedule_id"], name: "index_work_days_on_schedule_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
