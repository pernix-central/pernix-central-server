# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :v1 do
    get "vacation_request/index"
    get "vacation_request/user_vacations"
    get "vacation_request/show"
    post "vacation_request/create"
    put "vacation_request/update"
    delete "vacation_request/:id", to: "vacation_request#destroy"

    resources :vacation_request do
      put :update_status, on: :member
    end

    get "vacation/index"
    post "vacation/create"

    post "auth/login", to: "authentication#authenticate"

    post "auth/email-verify", to: "authentication#verify_email"

    resources :employees, except: %i[new edit delete], defaults: { format: "json" } do
      put :change_status, on: :member
      patch :change_password, on: :member
      patch :update_image, on: :member
      resources :schedules, only: %i[show update]
      post :password_recovery, on: :collection
      post :update_password, on: :collection
    end

    resources :suggestions, only: %i[index create show] do
      put "mark-resolved" => "suggestions#mark_resolved", as: :mark_resolved
      put "mark-unresolved" => "suggestions#mark_unresolved", as: :mark_unresolved
      resources :comments, only: %i[create]
    end

    resources :roles, only: %i[index]

    resources :checklist do
      get :user_tasks, on: :collection
    end

    resources :checklist_completion, only: %i[create destroy] do
      get :user_completions, on: :collection
    end

    resources :official_links

    resources :notification_employees, only: %i[update] do
      get :update_all, on: :collection
      get :notifications_messages, on: :collection
    end

    resources :notification_settings, only: %i[update] do
      get :show_employee_settings, on: :collection
    end

    resources :my_way_points_transactions, only: %i[create update destroy] do
      get :user_wallet, on: :collection
      get :wallet_summary, on: :collection
      put :update_wallets_status, on: :collection
      put :update_through_email, on: :member
    end

    resources :awards, only: %i[index]
  end
end
