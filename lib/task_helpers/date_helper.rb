# frozen_string_literal: true

# Helper for calculations related to dates
module DateHelper

  def add_days_and_exclude_weekends(days_to_add, date)
    days_added = 0
    current_date = date

    while days_added < days_to_add
      current_date += 1.day
      next if current_date.saturday? || current_date.sunday?

      days_added += 1
    end

    current_date
  end

end
