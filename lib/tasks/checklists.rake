# frozen_string_literal: true

namespace :checklist do
  desc "Clean checklist completions daily"
  task cleanup: :environment do
    Checklist::Completion.destroy_all
  end
end
