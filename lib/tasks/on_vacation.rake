# frozen_string_literal: true

namespace :on_vacation do

  desc "Check employee on vacation status"
  task check_on_vacation: :environment do
    Employee.all.update(on_vacation: false)
    vacation_request = Vacation.joins(:vacation_request).where(vacation_requests: { status: :Accepted },
                                                               date: "#{Time.zone.today} 00:00:00 CST -06:00")
                               .pluck(:employee_id)
    vacation_request.each do |vacation|
      Employee.find(vacation).update(on_vacation: true)
    end
  end

end
