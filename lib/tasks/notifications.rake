# frozen_string_literal: true

require "wrappers/sendgrid"
require "task_helpers/date_helper"

namespace :notifications do
  include Wrappers
  include DateHelper

  desc "Send all notifications"
  task send_all: :environment do
    Rake::Task["notifications:send_birthdays"].invoke
    Rake::Task["notifications:send_anniversaries"].invoke
    Rake::Task["notifications:send_request_reminders"].invoke
  end

  desc "Send birthday notifications"
  task send_birthdays: :environment do
    birthdays_today = Employee.where(active: true).select(&:birthday_today?).pluck(:id, :email, :first_name, :last_name)
    if birthdays_today.any?
      my_birthday_notification = Notification.find_by(notification_type: :my_birthday)
      birthdays_today.each do |employee_birthday|
        NotificationEmployee.create(employee_id: employee_birthday[0], notification_id: my_birthday_notification.id)
      end

      notification_birthday = Notification.find_by(notification_type: :birthday)
      employees = Employee.where(active: true).joins(:notification_setting).where("birthday->>'tray'=?", "true")
                          .pluck(:id, :email, :first_name, :last_name)
      employees.each do |employee|
        next if birthdays_today.include?(employee) && birthdays_today.length == 1

        names = birthdays_today.map do |employee_name|
          "#{employee_name[2]} #{employee_name[3]}" if employee_name != employee
        end
        names.delete(nil)
        NotificationEmployee.create(employee_id: employee[0], notification_id: notification_birthday.id,
                                    extra_information: names.to_s)
      end
    end
  end

  desc "Send anniversary notifications"
  task send_anniversaries: :environment do
    anniversaries_today = Employee.where(active: true).select(&:anniversary_today?)
                                  .pluck(:id, :email, :first_name, :last_name)
    if anniversaries_today.any?
      my_anniversary_notification = Notification.find_by(notification_type: :my_anniversary)
      anniversaries_today.each do |anniversary_employee|
        NotificationEmployee.create(employee_id: anniversary_employee[0],
                                    notification_id: my_anniversary_notification.id)
      end

      notification_anniversary = Notification.find_by(notification_type: :anniversary)
      employees = Employee.where(active: true).joins(:notification_setting).where("anniversary->>'tray'=?", "true")
                          .pluck(:id, :email, :first_name, :last_name)
      employees.each do |employee|
        next if anniversaries_today.include?(employee) && anniversaries_today.length == 1

        names = anniversaries_today.map do |employee_name|
          "#{employee_name[2]} #{employee_name[3]}" if employee_name != employee
        end
        names.delete(nil)
        NotificationEmployee.create(employee_id: employee[0], notification_id: notification_anniversary.id,
                                    extra_information: names.to_s)
      end
    end
  end

  desc "Send request reminders"
  task send_request_reminders: :environment do
    process_reminders(:wallet_request_reminder, MyWayPointsTransaction)
    process_reminders(:vacation_request_reminder, VacationRequest)
  end

  desc "Delete expired notifications"
  task delete_expired_notifications: :environment do
    expired_notification_ids =
      NotificationEmployee.expired_read.ids |
      NotificationEmployee.expired_birthday_types.ids |
      NotificationEmployee.expired_anniversary_types.ids |
      NotificationEmployee.expired_due_to_maximum_notification_timeframe.ids
    NotificationEmployee.where(id: expired_notification_ids).destroy_all
  end

  def process_reminders(reminder_type, class_type)
    Employee.joins(:role).where(roles: { name: :Admin }, active: true).each do |admin|
      notification_settings = admin.notification_setting.send(reminder_type)
      next unless notification_settings["notify_on"].include?(
        Date.current.strftime("%A"),
      )

      remove_all_submission_tray_notifications(reminder_type, admin.id)

      next if class_type.pending.empty?

      if notification_settings["tray"]
        notification_id = Notification.find_by(notification_type: reminder_type).id
        NotificationEmployee.create(notification_id: notification_id, employee_id: admin.id)
      end

      next if Rails.env.test? || !notification_settings["email"]

      send_reminder_email(reminder_type, admin)
    end
  end

  def remove_all_submission_tray_notifications(type, id)
    NotificationEmployee.joins(:notification).where(
      employee_id: id,
      notifications: {
        notification_type: type,
      },
    ).destroy_all
  end

  def send_reminder_email(type, admin)
    required_params = [admin.email, admin.first_name, admin.last_name]
    case type
    when :wallet_request_reminder
      Wrappers::Sendgrid.wallet_request_reminder(required_params)
    when :vacation_request_reminder
      Wrappers::Sendgrid.vacation_request_reminder(required_params)
    end
  end

end
