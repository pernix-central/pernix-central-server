# frozen_string_literal: true

namespace :employee do
  desc "Update seniority days to employees every year"
  task update_seniority_days: :environment do
    employees = Employee.where(active: true).select(&:anniversary_today?)
    employees.each do |employee|
      updated_seniority_days = employee.seniority_days + employee.employee_seniority_days
      employee.update(seniority_days: updated_seniority_days)
    end
  end
end
