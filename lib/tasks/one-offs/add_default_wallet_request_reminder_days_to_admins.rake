# frozen_string_literal: true

namespace :notifications do
  task add_default_wallet_request_reminder_days_to_admins: :environment do
    admins = Employee.joins(:role).where(roles: { name: :Admin }, active: true)

    admins.each do |admin|
      current_config = admin.notification_setting.wallet_request_reminder
      admin.notification_setting.update!(
        wallet_request_reminder: current_config.merge(
          { "notify_on" => Employee::DEFAULT_WALLET_REQUEST_REMINDER_DAYS },
        ),
      )
    end
  end
end
