# frozen_string_literal: true

namespace :notifications do
  task migrate_vacation_request_information_to_foreign_key: :environment do
    NotificationEmployee.joins(:notification).where(
      notifications: {
        notification_type: [
          Notification.notification_types[:vacation_request_submission],
          Notification.notification_types[:vacation_request_reminder],
          Notification.notification_types[:vacation_request_revision],
        ],
      },
    ).where.not(extra_information: nil).find_each do |notification|
      Rails.logger.info(
        "Updating record: #{notification.id} with vacation request id #{notification.extra_information}",
      )
      notification.update!(vacation_request_id: notification.extra_information.to_i)
    end
  end

  task add_new_admin_revision_category: :environment do
    Notification.create(
      notification_type: "admin_vacation_request_revision",
      content: "Vacation request for $1 has been reviewed",
      active: true,
    )
  end

  task remove_vacation_request_submission_notifications_that_have_expired: :environment do
    NotificationEmployee
      .joins(:notification)
      .joins(:vacation_request)
      .where(
        notifications: { notification_type: "vacation_request_submission" },
        vacation_requests: { status: "Accepted" },
      ).destroy_all
  end
end
