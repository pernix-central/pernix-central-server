# frozen_string_literal: true

require "uri"
require "net/http"

namespace :migrate do
  task migrate_paperclip_to_active_storage: :environment do
    Image.find_each do |image|
      next unless image.employee

      attachment = image.attachment

      if Rails.env.development? || Rails.env.test?
        image.employee.profile.attach(io: File.open(attachment.path), filename: attachment.original_filename)
      else
        url = URI.parse("https:#{attachment.url}")
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = (url.scheme == "https")
        request = Net::HTTP::Get.new(url.request_uri)
        response = http.request(request)

        if response.code == "200"
          image_io = StringIO.new(response.body)
          filename = File.basename(url.path)
          image.employee.profile.attach(io: image_io, filename: filename)
        else
          puts "Error getting the image: #{response.code} #{response.message}"
        end
      end
    end
  end
end
