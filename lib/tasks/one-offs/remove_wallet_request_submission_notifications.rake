# frozen_string_literal: true

namespace :notifications do
  task remove_wallet_request_submission_notifications: :environment do
    submission_type_id = Notification.find_by(notification_type: "wallet_point_request_submission").id

    NotificationEmployee
      .where(
        notification_id: submission_type_id,
      ).destroy_all
  end
end
