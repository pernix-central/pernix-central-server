# frozen_string_literal: true

require "sendgrid-ruby"

module Wrappers

  # Sending emails
  class Sendgrid < Module

    include SendGrid

    DEFAULT_FROM = "'Pernix Staff' <#{ENV['SENDGRID_DEFAULT_FROM']}>"

    def self.invite(employee, password)
      init_vars([[employee.email, employee.first_name, employee.last_name]])
      employee_name = "#{employee.first_name} #{employee.last_name}"

      @mail.template_id = ENV["SENDGRID_INVITE"]

      @personalization.add_dynamic_template_data(
        "employee" => employee_name,
        "email" => employee.email,
        "password" => password,
        "subject" => "#{employee_name}, welcome to Pernix!",
        "url" => "#{ENV['PC_CLIENT']}/login",
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.birthday_notifications(bday_employees, notification)
      image = notification.attachment.present? ? [notification.attachment.original] : []

      init_vars

      @mail.template_id = ENV["SENDGRID_BIRTHDAY_NOTIFICATION"]

      @personalization.add_dynamic_template_data(
        "employees" => bday_employees,
        "body" => notification.content,
        "image" => image,
        "subject" => notification.title,
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.anniversary_notifications(anniversary_employees, notification)
      image = notification.attachment.present? ? [notification.attachment.original] : []

      init_vars

      @mail.template_id = ENV["SENDGRID_BIRTHDAY_NOTIFICATION"]

      @personalization.add_dynamic_template_data(
        "employees" => anniversary_employees,
        "body" => notification.content,
        "image" => image,
        "subject" => notification.title,
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.schedule_update(employee, previous_schedule, previous_hours, new_schedule, new_hours)
      admins = Employee.joins(:role).where(roles: { name: :Admin }, active: true).joins(:notification_setting)
                       .where("change_schedule->>'email'=?", "true").pluck(:email, :first_name, :last_name)
      init_vars(admins)

      @mail.template_id = ENV["SENDGRID_SCHEDULE_CHANGE"]
      @personalization.add_dynamic_template_data(
        "employee" => "#{employee.first_name} #{employee.last_name}",
        "previous_hours" => previous_hours,
        "new_hours" => new_hours,
        "previous_schedule" => previous_schedule,
        "new_schedule" => new_schedule,
        "url" => "#{ENV['PC_CLIENT']}/employees/#{employee.id}/details",
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.vacation_request_submitted(vacation_request, vacation_dates)
      admins = Employee.joins(:role).where(roles: { name: :Admin }, active: true).joins(:notification_setting)
                       .where("vacation_request_submitted->>'email'=?", "true").pluck(:email, :first_name, :last_name)
      init_vars(admins)
      @mail.template_id = ENV["SENDGRID_VACATION_REQUEST_SUBMISSION"]
      base_url = "#{ENV['PC_CLIENT']}/vacations/#{vacation_request.employee.id}/vacation_history"
      @personalization.add_dynamic_template_data(
        "employee" => vacation_request.employee.full_name,
        "requested_dates" => vacation_dates,
        "amount" => vacation_dates.length,
        "client_approval" => vacation_request.client_approval,
        "accept_url" => "#{base_url}?request_id=#{vacation_request.id}&status=accepted",
        "deny_url" => "#{base_url}?request_id=#{vacation_request.id}&status=denied",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.revised_vacation_request(applicant, reviewer)
      employee = Employee.where(id: applicant.id).pluck(:email, :first_name, :last_name)
      init_vars(employee)
      @mail.template_id = ENV["SENDGRID_VACATION_REQUEST_REVISION"]
      @personalization.add_dynamic_template_data(
        "employee" => "#{reviewer.first_name} #{reviewer.last_name}",
        "url" => "#{ENV['PC_CLIENT']}/vacations/#{applicant.id}/vacation_history",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.admin_revised_vacation_request(request, admin)
      init_vars([admin].pluck(:email, :first_name, :last_name))

      @mail.template_id = ENV["SENDGRID_ADMIN_VACATION_REQUEST_REVISION"]
      @personalization.add_dynamic_template_data(
        "admin_name" => admin.first_name,
        "request_employee" => request.employee.full_name,
        "request_reviewer" => request.reviewer.full_name,
        "request_status" => request.status,
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.role_change(employee)
      send_employee = Employee.where(id: employee.id).pluck(:email, :first_name, :last_name)
      init_vars(send_employee)
      @mail.template_id = ENV["SENDGRID_ROLE_CHANGE"]
      @personalization.add_dynamic_template_data(
        "role" => employee.role.name.to_s,
        "url" => "#{ENV['PC_CLIENT']}/my-profile/#{employee.id}/details",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.suggestion_responses(suggestion)
      send_employee = Employee.where(id: suggestion.employee.id).pluck(:email, :first_name, :last_name)
      init_vars(send_employee)
      @mail.template_id = ENV["SENDGRID_SUGGESTION_RESPONSES"]
      @personalization.add_dynamic_template_data(
        "suggestion_name" => suggestion.title,
        "url" => "#{ENV['PC_CLIENT']}/suggestions/#{suggestion.id}/details",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.vacation_request_reminder(employee)
      init_vars([employee])
      pending_vacation_requests_data = VacationRequest.pending_vacation_requests.to_a
      @mail.template_id = ENV["SENDGRID_VACATION_REQUEST_REMINDER"]
      @personalization.add_dynamic_template_data(
        "url" => "#{ENV['PC_CLIENT']}/vacations/history",
        "pending_vacation_requests" => pending_vacation_requests_data,
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.revised_wallet_request(wallet_request)
      employee = Employee.where(id: wallet_request.employee.id).pluck(:email, :first_name, :last_name)
      init_vars(employee)
      @mail.template_id = ENV["SENDGRID_WALLET_REQUEST_REVISION"]
      @personalization.add_dynamic_template_data(
        "status" => wallet_request.status,
        "points" => wallet_request.request_points,
        "debit" => wallet_request.debit?,
        "reviewer" => wallet_request.reviewer.full_name,
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.pending_wallet_request_data
      base_url = "#{ENV['PC_CLIENT']}/wallet/summary"
      MyWayPointsTransaction.pending.order(:request_name).map do |request|
        {
          "employee_name" => request.employee.full_name,
          "points_requested" => request.request_points,
          "request_name" => request.request_name,
          "submission_date" => request.created_at.strftime("%b %d"),
          "days_since_submission" => (Date.current - request.created_at.to_date).to_i,
          "comments" => request.request_comments,
          "debit" => request.debit?,
          "accept_url" => "#{base_url}?request_id=#{request.id}&status=approved",
          "deny_url" => "#{base_url}?request_id=#{request.id}&status=denied",
        }
      end
    end

    def self.wallet_request_reminder(admin)
      init_vars([admin])
      wallet_request_data = pending_wallet_request_data
      @mail.template_id = ENV["SENDGRID_WALLET_REQUEST_REMINDER"]
      @personalization.add_dynamic_template_data(
        "url" => "#{ENV['PC_CLIENT']}/wallet/summary",
        "wallet_request_data" => wallet_request_data,
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.admin_revised_wallet_request(wallet_request, admin)
      init_vars([admin].pluck(:email, :first_name, :last_name))

      @mail.template_id = ENV["SENDGRID_ADMIN_WALLET_REQUEST_REVISION"]
      @personalization.add_dynamic_template_data(
        "admin_name" => admin.first_name,
        "request_employee" => wallet_request.employee.full_name,
        "request_reviewer" => wallet_request.reviewer.full_name,
        "request_status" => wallet_request.status,
      )

      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.request_removal(admin, request, request_type)
      init_vars([admin].pluck(:email, :first_name, :last_name))
      @mail.template_id = ENV["SENDGRID_REQUEST_REMOVAL"]
      @personalization.add_dynamic_template_data(
        "admin_name" => admin.first_name,
        "submitter" => request.employee.full_name,
        "submission_date" => request.created_at.to_date,
        "request_type" => request_type,
        "subject" => "#{request_type.capitalize} request withdrawn!",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    def self.reset_password(employee)
      init_vars([employee].pluck(:email, :first_name, :last_name))
      @mail.template_id = ENV["SENDGRID_RESET_PASSWORD"]
      @personalization.add_dynamic_template_data(
        "employee" => employee.full_name,
        "reset_link" => "#{ENV['PC_CLIENT']}/reset-password/#{employee.reset_token}",
      )
      @mail.add_personalization(@personalization)
      dispatch(@mail)
    end

    class << self

      include SendGrid

      private

      def init_vars(employees = Employee.where(active: true).pluck(:email, :first_name, :last_name))
        @mail = SendGrid::Mail.new
        @mail.from = SendGrid::Email.new(email: DEFAULT_FROM)
        @personalization = Personalization.new

        employees.each do |e|
          @personalization.add_to(Email.new(email: e[0], name: "#{e[1]} #{e[2]}"))
        end
      end

      def dispatch(email)
        sg = SendGrid::API.new(api_key: ENV["SENDGRID_API_KEY"], host: ENV["SENDGRID_URL"])
        response = sg.client.mail._("send").post(request_body: email.to_json)
        Rails.logger.debug response.body.to_s
      end

    end

  end

end
