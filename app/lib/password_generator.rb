# frozen_string_literal: true

# Class to generate passwords
class PasswordGenerator

  def self.generate
    (0...8).map { rand(65..90).chr }.join
  end

end
