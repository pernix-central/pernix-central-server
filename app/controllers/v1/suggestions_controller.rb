# frozen_string_literal: true

# Controller for suggestions
class V1::SuggestionsController < ApplicationController

  before_action :find_suggestion, only: %i[show]

  def index
    @suggestions = Suggestion.all
    render json: @suggestions, include: %w[comments]
  end

  def show
    render json: @suggestion, include: %w[comments]
  end

  def create
    @suggestion = Suggestion.new(suggestion_params)

    if @suggestion.save
      render json: @suggestion
    else
      render json: @suggestion.errors
    end
  end

  def mark_resolved
    @suggestion = Suggestion.find(params[:suggestion_id])
    if @suggestion.update(management_resolved: true)
      render json: @suggestion, include: %w[comments]
    else
      render json: @suggestion.errors
    end
  end

  def mark_unresolved
    @suggestion = Suggestion.find(params[:suggestion_id])
    if @suggestion.update(management_resolved: false)
      render json: @suggestion, include: %w[comments]
    else
      render json: @suggestion.errors
    end
  end

  private

  def suggestion_params
    params.require(:suggestion).permit(
      :title, :description, :management_resolved, :employee_id
    )
  end

  def find_suggestion
    @suggestion = Suggestion.find(params[:id])
  end

end
