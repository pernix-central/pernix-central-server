# frozen_string_literal: true

# Controller for official links
class V1::OfficialLinksController < ApplicationController

  before_action :find_official_link, only: %i[show update destroy]

  def index
    render json: OfficialLink.all.order("name ASC"), each_serializer: OfficialLinkSerializer
  end

  def show
    render json: @official_link, each_serializer: OfficialLinkSerializer
  end

  def create
    @official_link = OfficialLink.new(official_link_params)

    if @official_link.save
      json_response(@official_link)
    else
      json_response(@official_link.errors, :unprocessable_entity)
    end
  end

  def update
    if @official_link.update(official_link_params)
      json_response(@official_link)
    else
      json_response(@official_link, :unprocessable_entity)
    end
  end

  def destroy
    @official_link.destroy
    render json: :ok
  end

  private

  def find_official_link
    @official_link = OfficialLink.find(params[:id])
  end

  def official_link_params
    params.require(:official_link).permit(
      :name, :hyperlink
    )
  end

end
