# frozen_string_literal: true

# Controller for vacation
class V1::VacationController < ApplicationController

  def index
    @schedule = Vacation.find(params[:id])
  end

  def create
    date = permit_params[:vacation_dates_input_list][0][:date]
    @vacation = Vacation.new({ vacation_request_id: permit_params[:vacation_request_id], date: date })
    if @vacation
      @vacation.save
      render json: @vacation
    else
      render json: @vacation.errors
    end
  end

  def permit_params
    params.permit(:vacation_request_id, :vacation_dates_input_list)
  end

end
