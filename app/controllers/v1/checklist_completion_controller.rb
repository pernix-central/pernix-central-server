# frozen_string_literal: true

# Controller for daily checklist completions
class V1::ChecklistCompletionController < ApplicationController

  before_action :find_task_completion, only: %i[destroy]

  def create
    @checklist_task = find_task
    if @checklist_task.nil?
      render json: :no_content
    else
      @checklist_completion = Checklist::Completion.new(employee_id: current_employee.id,
                                                        task_id: @checklist_task.id)
      if @checklist_completion.save
        render json: @checklist_completion
      else
        render json: @checklist_completion.errors
      end
    end
  end

  def destroy
    @checklist_completion.destroy
    render json: :ok
  end

  def user_completions
    @user_completions = Checklist::Completion.where(employee_id: current_employee.id)
    render json: @user_completions, each_serializer: ChecklistCompletionSerializer
  end

  private

  def checklist_completion_params
    params.require(:checklist_completion).permit(
      :task_id,
    )
  end

  def find_task
    @checklist_task = Checklist::Task.find_by(id: params[:checklist][:id])
  end

  # Finds the task completion that matches the task id and employee id
  def find_task_completion
    @checklist_completion = Checklist::Completion.find_by(task_id: params[:id], employee_id: current_employee.id)
  end

end
