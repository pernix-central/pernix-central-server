# frozen_string_literal: true

# Controller for notification settigns
class V1::NotificationSettingsController < ApplicationController

  before_action :find_notification_setting, only: %i[update]

  def show_employee_settings
    render json: NotificationSetting.find_by(employee_id: current_employee.id),
           each_serializer: NotificationSettingSerializer
  end

  def update
    if @notification_setting.update(notification_setting_params)
      json_response(@notification_setting)
    else
      json_response(@notification_setting.errors, :unprocessable_entity)
    end
  end

  private

  def find_notification_setting
    @notification_setting = NotificationSetting.find(params[:id])
  end

  def notification_setting_params
    params.require(:notification_setting).permit(
      anniversary: %i[email tray],
      birthday: %i[email tray],
      change_role: %i[email tray],
      vacation_request_reviewed: %i[email tray],
      vacation_request_submitted: %i[email tray],
      vacation_request_reminder: [:email, :tray, { notify_on: [] }],
      wallet_request_reminder: [:email, :tray, { notify_on: [] }],
      wallet_request_reviewed: %i[email tray],
      suggestion_responses: %i[email tray],
      change_schedule: %i[email tray],
      request_removal: %i[email tray],
    )
  end

end
