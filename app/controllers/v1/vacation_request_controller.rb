# frozen_string_literal: true

# Controller for vacaion request
class V1::VacationRequestController < ApplicationController

  def index
    render json: VacationRequest.all.order("employee_id ASC"), each_serializer: VacationRequestsSerializer
  end

  def user_vacations
    @vacation_request = VacationRequest.where(employee_id: params[:employeeid])
    render json: @vacation_request, each_serializer: VacationRequestsSerializer, include: ["vacations"]
  end

  def create
    @vacation_request = VacationRequest.new(vacation_request_params)
    if @vacation_request
      @vacation_request.save
      create_vacations(@vacation_request.id, params[:vacation_dates_input_list])
      send_request_notification
      render json: @vacation_request
    else
      render json: @vacation_request.errors
    end
  end

  def create_vacations(vacation_request_id, vacations)
    @vacation_dates = []
    vacations.each do |vacation|
      Vacation.create({ vacation_request_id: vacation_request_id, date: vacation[:date] })
      @vacation_dates.push(vacation[:date].gsub!("T06:00:00.000Z", ""))
    end
  end

  def update
    @vacation_request = VacationRequest.find(params[:id])

    if @vacation_request.update(vacation_request_params)
      @vacation_request.reload
      send_update_notification
      render json: @vacation_request
    else
      render json: @vacation_request.errors
    end
  end

  def destroy
    @vacation_request = VacationRequest.find(params[:id])
    @vacation_request.destroy

    if @vacation_request.destroyed?
      remove_related_request_submission_notifications
      send_request_removal_notifications
    end

    render json: :ok
  end

  def remove_related_request_submission_notifications
    NotificationEmployee
      .where(notifyable_id: @vacation_request.id, notifyable_type: "VacationRequest")
      .destroy_all
  end

  def send_request_removal_notifications
    Employee.joins(:role).where(roles: { name: "Admin" }, active: true).each do |admin|
      if admin.notification_setting.request_removal["tray"]
        extra_information =
          "#{@vacation_request.employee.full_name},vacation"
        notification_id = Notification.find_by(notification_type: "request_removal").id
        NotificationEmployee.create(employee_id: admin.id, notification_id: notification_id,
                                    notifyable_id: @vacation_request.id, notifyable_type: "VacationRequest",
                                    extra_information: extra_information)
      end

      next if Rails.env.test? || !admin.notification_setting.request_removal["email"]

      Wrappers::Sendgrid.request_removal(admin, @vacation_request, "vacation")
    end
  end

  def update_status
    @vacation_request = VacationRequest.find(params[:id])

    if @vacation_request.status.humanize != VacationRequest.statuses["pending"]
      render json: { message: "Vacation request has already been #{@vacation_request.status}." }, status: :ok
    elsif @vacation_request.update(
      status: VacationRequest.statuses[params[:status]],
      reviewer_id: params["reviewer_id"],
      review_date: Time.now.utc,
    )
      @vacation_request.reload
      send_update_notification
      render json: { message: "Vacation request successfully #{params[:status]}" }, status: :ok
    else
      render json: { error: "An error has occurred and the vacation request could not be accepted or denied." },
             status: :unprocessable_entity
    end
  end

  def vacation_request_params
    params.require(:vacation_request).permit(
      :id,
      :requested_date,
      :client_approval,
      :status,
      :review_date,
      :employee_id,
      :reviewer_id,
    )
  end

  private

  def send_request_notification
    Wrappers::Sendgrid.vacation_request_submitted(@vacation_request, @vacation_dates) unless Rails.env.test?

    employees = Employee.joins(:role).where(roles: { name: :Admin }, active: true)
                        .joins(:notification_setting).where("vacation_request_submitted->>'tray'=?", "true")
                        .pluck(:id, :email, :first_name, :last_name)
    notification = Notification.find_by(notification_type: :vacation_request_submission)
    employees.each do |e|
      next unless e[0] != @vacation_request.employee_id

      NotificationEmployee.create(notification_id: notification.id, employee_id: e[0],
                                  sender_id: @vacation_request.employee_id, notifyable: @vacation_request,
                                  notifyable_type: "VacationRequest")
    end
  end

  def send_update_notification
    return unless @vacation_request.employee_id != @vacation_request.reviewer_id && @vacation_request.employee.active

    remove_related_vacation_request_submissions(@vacation_request.id)
    create_vacation_review_notifications_for_admins

    employee_notification_settings = @vacation_request.employee.notification_setting
    employee_tray_and_email_settings = {
      tray: employee_notification_settings.vacation_request_reviewed["tray"],
      email: employee_notification_settings.vacation_request_reviewed["email"],
    }

    create_employee_notifications(employee_tray_and_email_settings)
  end

  def create_employee_notifications(notification_settings)
    if notification_settings[:tray]
      notification = Notification.find_by(notification_type: :vacation_request_revision)
      NotificationEmployee.create(notification_id: notification.id, employee_id: @vacation_request.employee_id,
                                  sender_id: @vacation_request.reviewer_id, notifyable: @vacation_request,
                                  notifyable_type: "VacationRequest")
    end

    return if Rails.env.test? || !notification_settings[:email]

    Wrappers::Sendgrid.revised_vacation_request(@vacation_request.employee,
                                                @vacation_request.reviewer)
  end

  def remove_related_vacation_request_submissions(id)
    NotificationEmployee
      .joins(:notification)
      .where(notifications: { notification_type: "vacation_request_submission" }, notifyable_id: id,
             notifyable_type: "VacationRequest")
      .destroy_all
  end

  def create_vacation_review_notifications_for_admins
    admin_notification = Notification.find_by(notification_type: :admin_vacation_request_revision)
    admins = Employee.joins(:role).where(roles: { name: :Admin }, active: true)
                     .joins(:notification_setting).where.not(email: @vacation_request.reviewer.email)

    admins.each do |admin|
      if admin.notification_setting.vacation_request_reviewed["tray"]
        NotificationEmployee.create(notification_id: admin_notification.id, employee_id: admin.id,
                                    sender_id: @vacation_request.employee_id, notifyable: @vacation_request,
                                    notifyable_type: "VacationRequest")
      end

      next if Rails.env.test? || !admin.notification_setting.vacation_request_reviewed["email"]

      Wrappers::Sendgrid.admin_revised_vacation_request(@vacation_request, admin)
    end
  end

end
