# frozen_string_literal: true

# Controller for roles
class V1::RolesController < ApplicationController

  def index
    @roles = Role.all
    render json: @roles
  end

end
