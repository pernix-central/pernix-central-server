# frozen_string_literal: true

# Controller for daily checklist
class V1::ChecklistController < ApplicationController

  before_action :find_checklist_task, only: %i[show update destroy]

  def index
    @checklist = Checklist::Task.all
    render json: @checklist, each_serializer: ChecklistSerializer
  end

  def show
    if @checklist_task.nil?
      render json: :no_content
    else
      render json: @checklist_task, each_serializer: ChecklistSerializer
    end
  end

  def user_tasks
    user_tasks = Checklist::Task.by_employee(current_employee.id)
    user_completions = current_employee.completions
    render json: { tasks: user_tasks, completions: user_completions }
  end

  def create
    @checklist_task = Checklist::Task.new(checklist_task_params)
    if @checklist_task.save
      render json: @checklist_task
    else
      render json: @checklist_task.errors
    end
  end

  def update
    if @checklist_task.update(checklist_task_params)
      render json: @checklist_task
    else
      render json: @checklist_task.errors
    end
  end

  def destroy
    @checklist_task.destroy
    render json: :ok
  end

  private

  def find_checklist_task
    @checklist_task = Checklist::Task.find(params[:id])
  end

  def checklist_task_params
    params.require(:checklist).permit(
      :task_name,
      :task_type,
      :employee_id,
    )
  end

end
