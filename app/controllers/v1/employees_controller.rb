# frozen_string_literal: true

# Controller for employees
class V1::EmployeesController < ApplicationController

  skip_before_action :authorize_request, only: %i[password_recovery update_password]
  before_action :find_employee, only: %i[show update update_image change_password change_status]

  def index
    @employees = if current_employee.role_id == 1
                   Employee.all.order("first_name ASC")
                 else
                   Employee.where(active: true).order("first_name ASC")
                 end
    if from_pernix_site?
      render json: @employees, each_serializer: PernixSiteEmployeeSerializer
    else
      render json: @employees, each_serializer: EmployeeSerializer, include: ["schedule.work_days.periods"]
    end
  end

  def show
    render json: @employee,
           include: ["schedule.work_days.periods", "role"]
  end

  def create
    employee = Employee.new(employee_params)
    pw = PasswordGenerator.generate
    employee.password = pw
    if employee.save
      employee_auth = EmployeeAuthentication.new(employee.email, employee.password)
      employee_auth.authenticate
      response = { message: Message.account_created, auth_token: employee_auth.token }

      Wrappers::Sendgrid.invite(employee, pw) unless Rails.env.test? || Rails.env.development?

      json_response(response, :created)
    else
      json_response(employee.errors, :unprocessable_entity)
    end
  end

  def update
    previous_role_id = @employee.role.id
    if @employee.update(employee_params)
      role_change(previous_role_id) if previous_role_id != @employee.role.id
      render json: @employee, include: ["schedule.work_days.periods", "role"]
    else
      render json: @employee.errors
    end
  end

  def change_password
    if changing_password?
      if @employee.update(password_params)
        render json: @employee
      else
        render json: @employee.errors
      end
    else
      json_response(@employee.errors, :unprocessable_entity)
    end
  end

  def change_status
    if @employee.update(status_params)
      render json: @employee, include: ["schedule.work_days.periods", "role"]
    else
      render json: @employee.errors
    end
  end

  def password_recovery
    email = params[:email]

    if email.present?
      employee = Employee.find_by(email: email)

      if employee
        if employee.reset_token.nil? || !employee.reset_token_valid?
          employee.generate_reset_token
          Wrappers::Sendgrid.reset_password(employee)
          render json: { message: "Password recovery initiated successfully. Check your email for instructions." },
                 status: :ok
        else
          render json: { message: "Password recovery email already sent. Please check your email for instructions." },
                 status: :ok
        end
      else
        render json: { error_message: "No user found with this email." }, status: :unprocessable_entity
      end
    else
      render json: { error_message: "Email is required" }, status: :unprocessable_entity
    end
  end

  def update_password
    employee = Employee.find_by(reset_token: params[:reset_token])

    if employee&.reset_token_valid?
      if params[:new_password].present?
        employee.password = params[:new_password]
        employee.password_confirmation = params[:confirm_password]

        if employee.save
          employee.clear_reset_token
          render json: { message: "Password Updated." }, status: :ok
        else
          render json: { message: "Error updating password." }, status: :unprocessable_entity
        end
      else
        render json: { message: "Error updating password. New password is missing." }, status: :unprocessable_entity
      end
    else
      render json: { message: "Invalid reset token." }, status: :unprocessable_entity
    end
  end

  def update_image
    if params[:employee][:profile].present?
      if params[:employee][:profile] != "remove"
        @employee.profile.attach(params[:employee][:profile])
      else
        @employee.profile.purge
      end
      render json: @employee, include: ["schedule.work_days.periods", "role"]
    else
      json_response(@employee.errors, :unprocessable_entity)
    end
  end

  private

  def role_change(previous_role_id)
    @employee.add_default_admin_reminder_setting if previous_role_id == 2 && @employee.role.id == 1
    @employee.remove_admin_reminder_setting if previous_role_id == 1 && @employee.role.id == 2
    send_role_notification
  end

  def send_role_notification
    return unless current_employee.id != @employee.id && @employee.active

    Wrappers::Sendgrid.role_change(@employee) if @employee.notification_setting.change_role["email"] && !Rails.env.test?

    return unless @employee.notification_setting.change_role["tray"]

    notification = Notification.find_by(notification_type: :role_change)
    NotificationEmployee.create(notification_id: notification.id, employee_id: @employee.id,
                                sender_id: current_employee.id, extra_information: @employee.role.name)
  end

  def from_pernix_site?
    params[:pernix_site].present? && params[:pernix_site] == "true"
  end

  def status_params
    params.require(:employee).permit(:active)
  end

  def password_params
    params.require(:employee).permit(:password, :password_confirmation)
  end

  def image_params
    params.require(:employee).permit(:profile)
  end

  def find_employee
    @employee = Employee.find(params[:id])
  end

  def employee_params
    params.require(:employee).permit(
      :first_name, :last_name, :birthdate, :email, :active,
      :has_car, :blog_url, :computer_serial_number, :mac_address,
      :hire_date, :on_vacation, :can_be_mentor, :vacation_control,
      :phone_number, :id_number, :role_id, :reset_token, :profile
    )
  end

  def changing_password?
    params[:employee][:password].present? &&
      params[:employee][:password_confirmation].present? &&
      params[:employee][:password] == params[:employee][:password_confirmation]
  end

end
