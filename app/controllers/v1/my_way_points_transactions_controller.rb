# frozen_string_literal: true

# Controller responsible for handling API endpoints related to MyWayPointsTransaction.
class V1::MyWayPointsTransactionsController < ApplicationController

  def user_wallet
    @user_wallet = MyWayPointsTransaction.where(employee_id: params[:employee_id]).order("created_at DESC")
    render json: @user_wallet, each_serializer: MyWayPointsTransactionSerializer
  end

  def wallet_summary
    @wallet_summary = MyWayPointsTransaction.where(request_type: :credit, status: :pending).order("request_name ASC")
    render json: @wallet_summary, each_serializer: MyWayPointsTransactionSerializer
  end

  def create
    @wallet_request = MyWayPointsTransaction.new(wallet_request_params)
    if @wallet_request.save
      render json: @wallet_request, status: :created
    else
      render json: { errors: @wallet_request.errors }, status: :unprocessable_entity
    end
  end

  def update
    @wallet_request = MyWayPointsTransaction.find(params[:id])

    if @wallet_request.update(wallet_request_params)
      @wallet_request.reload
      send_request_update_notifications
      render json: @wallet_request
    else
      render json: @wallet_request.errors
    end
  end

  def update_through_email
    @wallet_request = MyWayPointsTransaction.find(params[:id])

    if @wallet_request.status.humanize != MyWayPointsTransaction.statuses["pending"]
      render json: { message: "Wallet request has already been #{@wallet_request.status}." }, status: :ok
    elsif @wallet_request.update!(
      status: MyWayPointsTransaction.statuses[params[:status]],
      reviewer_id: params["reviewer_id"],
      review_date: Time.now.utc,
    )
      @wallet_request.reload
      send_request_update_notifications
      render json: { message: "Wallet request successfully #{params[:status]}" }, status: :ok
    else
      render json: { error: "An error has occurred and the wallet request could not be accepted or denied." },
             status: :unprocessable_entity
    end
  end

  def update_wallets_status
    if update_status_params?
      wallet_ids = params[:ids]

      begin
        wallet_ids.each do |wallet_id|
          @wallet_request = MyWayPointsTransaction.find(wallet_id)
          @wallet_request.update!(status: MyWayPointsTransaction.statuses[params[:status]],
                                  reviewer_id: current_employee.id,
                                  review_date: Time.now.utc)
          @wallet_request.reload
          send_request_update_notifications
        end
        render json: { message: "Wallet requests successfully #{params[:status]}." }, status: :ok
      rescue ActiveRecord::StatementInvalid
        render json: { error: "An error has occurred and the wallet requests could not be updated." },
               status: :unprocessable_entity
      end
    else
      render json: { error: "Invalid parameters number." }, status: :unprocessable_entity
    end
  end

  def destroy
    @wallet_request = MyWayPointsTransaction.find(params[:id])
    @wallet_request.destroy
    send_request_removal_notifications if @wallet_request.destroyed?

    render json: :ok
  end

  private

  def send_request_removal_notifications
    Employee.joins(:role).where(roles: { name: "Admin" }, active: true).each do |admin|
      if admin.notification_setting.request_removal["tray"]
        request_type = @wallet_request.debit? ? "wallet point redemption" : "wallet point"
        extra_information =
          "#{@wallet_request.employee.full_name},#{request_type}"
        notification_id = Notification.find_by(notification_type: "request_removal").id
        NotificationEmployee.create(employee_id: admin.id, notification_id: notification_id,
                                    notifyable_id: @wallet_request.id, notifyable_type: "MyWayPointsTransaction",
                                    extra_information: extra_information)
      end

      next if Rails.env.test? || !admin.notification_setting.request_removal["email"]

      Wrappers::Sendgrid.request_removal(admin, @wallet_request, request_type)
    end
  end

  def send_request_update_notifications
    return unless @wallet_request.employee_id != @wallet_request.reviewer_id &&
                  @wallet_request.employee.active &&
                  !Rails.env.test?

    create_wallet_request_review_notifications_for_admins

    employee_notification_settings = @wallet_request.employee.notification_setting
    employee_tray_and_email_settings = {
      tray: employee_notification_settings.wallet_request_reviewed["tray"],
      email: employee_notification_settings.wallet_request_reviewed["email"],
    }

    create_employee_notifications(employee_tray_and_email_settings)
  end

  def create_wallet_request_review_notifications_for_admins
    admin_notification = Notification.find_by(notification_type: :admin_wallet_request_revision)
    admins = Employee.joins(:role).where(roles: { name: :Admin }, active: true)
                     .joins(:notification_setting).where.not(email: @wallet_request.reviewer.email)

    admins.each do |admin|
      if admin.notification_setting.wallet_request_reviewed["tray"]
        NotificationEmployee.create(notification_id: admin_notification.id, employee_id: admin.id,
                                    sender_id: @wallet_request.employee_id, notifyable: @wallet_request,
                                    notifyable_type: "MyWayPointsTransaction")
      end

      next unless admin.notification_setting.wallet_request_reviewed["email"]

      Wrappers::Sendgrid.admin_revised_wallet_request(@wallet_request, admin)
    end
  end

  def create_employee_notifications(notification_settings)
    notification_type = @wallet_request.wallet_request_revision_notification_type
    wallet_notification = Notification.find_by(notification_type: notification_type)
    if notification_settings[:tray]
      NotificationEmployee.create(notification_id: wallet_notification.id, employee_id: @wallet_request.employee_id,
                                  sender_id: @wallet_request.reviewer_id, notifyable: @wallet_request,
                                  notifyable_type: "MyWayPointsTransaction")
    end

    return if Rails.env.test? || !notification_settings[:email]

    Wrappers::Sendgrid.revised_wallet_request(@wallet_request)
  end

  def wallet_request_params
    params.require(:wallet_request).permit(
      :id,
      :request_name,
      :request_points,
      :request_type,
      :request_comments,
      :status,
      :review_date,
      :employee_id,
      :reviewer_id,
    )
  end

  def update_status_params?
    params[:ids].present? && params[:status].present?
  end

end
