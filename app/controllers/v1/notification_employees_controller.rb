# frozen_string_literal: true

# Controller for notification employees
class V1::NotificationEmployeesController < ApplicationController

  before_action :find_notification_employee, only: %i[update]
  before_action :find_notifications, only: %i[notifications_messages update_all]

  def notifications_messages
    render json: @employee_notifications, each_serializer: NotificationEmployeeSerializer
  end

  def update
    if @notification_employee.update(read: true)
      json_response(@notification_employee)
    else
      json_response(@notification_employee.errors, :unprocessable_entity)
    end
  end

  def update_all
    if @employee_notifications.update(read: true)
      json_response(@employee_notifications)
    else
      json_response(@employee_notifications.errors, :unprocessable_entity)
    end
  end

  private

  def find_notification_employee
    @notification_employee = NotificationEmployee.find(params[:id])
  end

  def find_notifications
    @employee_notifications = NotificationEmployee.where(employee_id: current_employee.id).order("created_at DESC")
  end

end
