# frozen_string_literal: true

# Controller responsible for handling API endpoints related to Award.
class V1::AwardsController < ApplicationController

  def index
    awards = if current_employee.role_id == 3
               Award.where(apprentice: true).order(name: :asc)
             else
               Award.all.order(name: :asc)
             end

    render json: awards, each_serializer: AwardSerializer
  end

end
