# frozen_string_literal: true

# Controller for schedules
class V1::SchedulesController < ApplicationController

  before_action :find_schedule

  def show
    render json: @schedule, include: "work_days.periods"
  end

  def update
    @previous_schedule = get_organized_schedule(@schedule)
    @previous_hours = @schedule.total_work_hours.to_i
    if @schedule.update(schedule_params)
      @schedule.update_work_hours
      send_schedule_notification if schedule_changed?

      render json: @schedule.employee, include: "schedule.work_days.periods"
    else
      render json: @schedule.errors, status: :unprocessable_entity
    end
  end

  private

  def send_schedule_notification
    unless Rails.env.test?
      Wrappers::Sendgrid.schedule_update(@schedule.employee, @previous_schedule, @previous_hours, @new_schedule,
                                         @new_hours)
    end

    notification = Notification.find_by(notification_type: :change_schedule)
    employees = Employee.joins(:role).where(roles: { name: :Admin }, active: true)
                        .joins(:notification_setting).where("change_schedule->>'tray'=?", "true")
                        .pluck(:id, :email, :first_name, :last_name)

    employees.each do |e|
      if e[0] != @schedule.employee_id
        NotificationEmployee.create(notification_id: notification.id, employee_id: e[0],
                                    sender_id: @schedule.employee_id)
      end
    end
  end

  def schedule_changed?
    @new_schedule = get_organized_schedule(@schedule)
    @new_hours = @schedule.total_work_hours.to_i
    @previous_schedule != @new_schedule
  end

  def get_organized_schedule(schedule)
    organized_schedule = []
    schedule.work_days.each do |day|
      schedule_hash = {}
      schedule_hash["day"] = day.week_day
      periods_subarray = []
      day.periods.each do |period|
        periods_subarray += [" #{period.start_time.to_s(:time)}-#{period.end_time.to_s(:time)} "]
      end
      schedule_hash["periods"] = periods_subarray
      organized_schedule += [schedule_hash]
    end
    organized_schedule
  end

  def find_schedule
    @schedule = Schedule.find(params[:id])
  end

  def schedule_params
    params.require(:schedule).permit(
      work_days_attributes: [
        :id, :wfh, :week_day, :_destroy,
        { periods_attributes: %i[
          id start_time end_time _destroy
        ] }
      ],
    )
  end

end
