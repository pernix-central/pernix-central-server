# frozen_string_literal: true

# Controller for comments
class V1::CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_params)

    if @comment.save
      send_comment_notification unless Rails.env.test?
      render json: @comment
    else
      render json: @comment.errors
    end
  end

  private

  def comment_params
    params.require(:comment).permit(
      :text, :employee_id, :suggestion_id, :anonymus
    )
  end

  def send_comment_notification
    employee = @comment.suggestion.employee
    return unless current_employee.id != employee.id && employee.active

    if @comment.suggestion.employee.notification_setting.suggestion_responses["email"]
      Wrappers::Sendgrid.suggestion_responses(@comment.suggestion)
    end

    return unless @comment.suggestion.employee.notification_setting.suggestion_responses["tray"]

    notification = Notification.find_by(notification_type: :suggestion_responses)
    NotificationEmployee.create(notification_id: notification.id, employee_id: @comment.suggestion.employee_id,
                                extra_information: @comment.suggestion_id)
  end

end
