# frozen_string_literal: true

# Controller for authentication
class V1::AuthenticationController < ApplicationController

  skip_before_action :authorize_request, only: %i[authenticate verify_email]
  # return auth token once employee is authenticated
  def authenticate
    employee_auth = EmployeeAuthentication.new(auth_params[:email], auth_params[:password])
    employee_auth.authenticate

    if employee_auth.employee.active
      render json: employee_auth,
             include: ["employee", "employee.schedule.work_days.periods", "employee.role"]
    else
      response = { message: Message.unauthorized }
      json_response(response, :unauthorized)
    end
  end

  def verify_email
    employee_authentication = EmployeeAuthentication.new(verify_params[:email], "")
    employee_authentication.verify_email

    if employee_authentication.employee.active
      render json: employee_authentication,
             include: ["employee", "employee.schedule.work_days.periods", "employee.role"]
    else
      response = { message: Message.unauthorized }
      json_response(response, :unauthorized)
    end
  end

  private

  def auth_params
    params.permit(:email, :password, authentication: %i[email password])
  end

  def verify_params
    params.permit(:email)
  end

end
