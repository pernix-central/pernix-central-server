# frozen_string_literal: true

require "wrappers/sendgrid"

# Application controller
class ApplicationController < ActionController::API

  include Response
  include ExceptionHandler
  include Wrappers

  before_action :authorize_request
  attr_reader :current_employee

  private

  # Check for valid request token and return employee
  def authorize_request
    @current_employee = AuthorizeApiRequest.new(request.headers).call[:employee]
  end

end
