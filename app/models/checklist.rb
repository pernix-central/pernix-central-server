# frozen_string_literal: true

# Class to record checklist
module Checklist

  def self.table_name_prefix
    "checklist_"
  end

end
