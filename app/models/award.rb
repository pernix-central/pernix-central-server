# frozen_string_literal: true

# Class to record Awards
class Award < ApplicationRecord

  validates :name, :points, :apprentice, presence: true

end
