# frozen_string_literal: true

# Class to record employees
class Employee < ApplicationRecord

  has_secure_password

  validates :first_name, :last_name, :email, :password_digest, :birthdate, :id_number, :phone_number, :hire_date,
            presence: true

  validates :email, :id_number, uniqueness: true

  has_one :schedule, dependent: :destroy
  has_one :notification_setting, dependent: :destroy

  belongs_to :role

  has_one_attached :profile

  has_many :suggestions, dependent: :destroy

  has_many :notification_employees, dependent: :destroy
  has_many :notifications, through: :notification_employees

  has_many :notification_sender, class_name: "NotificationEmployee", foreign_key: "sender_id", inverse_of: Employee,
                                 dependent: :destroy

  has_many :tasks, class_name: "Checklist::Task", dependent: :destroy
  has_many :completions, class_name: "Checklist::Completion", dependent: :destroy

  has_many :my_way_points_transaction_requests, class_name: "MyWayPointsTransaction", dependent: :destroy
  has_many :my_way_points_transaction_reviews, class_name: "MyWayPointsTransaction",
                                               foreign_key: "reviewer_id", inverse_of: Employee, dependent: :destroy

  has_many :vacation_requests, class_name: "VacationRequest", dependent: :destroy
  has_many :vacation_reviews, class_name: "VacationRequest", foreign_key: "reviewer_id", inverse_of: Employee,
                              dependent: :destroy

  after_create :default_schedule
  after_create :default_notification_setting
  after_create :add_default_admin_reminder_setting, if: :admin?

  accepts_nested_attributes_for :schedule
  accepts_nested_attributes_for :notification_setting

  DEFAULT_VACATION_REQUEST_REMINDER_DAYS = %w[Monday Thursday].freeze
  DEFAULT_WALLET_REQUEST_REMINDER_DAYS = %w[Monday Tuesday Wednesday Thursday Friday].freeze

  def admin?
    role&.name == "Admin"
  end

  def add_default_admin_reminder_setting
    current_vacation_config = notification_setting.vacation_request_reminder
    current_wallet_config = notification_setting.wallet_request_reminder

    notification_setting.update!(
      vacation_request_reminder: current_vacation_config.merge(
        { "notify_on" => DEFAULT_VACATION_REQUEST_REMINDER_DAYS },
      ),
      wallet_request_reminder: current_wallet_config.merge(
        { "notify_on" => DEFAULT_WALLET_REQUEST_REMINDER_DAYS },
      ),
    )
  end

  def remove_admin_reminder_setting
    current_vacation_config = notification_setting.vacation_request_reminder
    current_wallet_config = notification_setting.wallet_request_reminder

    notification_setting.update!(
      vacation_request_reminder: current_vacation_config.except("notify_on"),
      wallet_request_reminder: current_wallet_config.except("notify_on"),
    )
  end

  def full_name
    "#{first_name.strip} #{last_name.strip}"
  end

  def birthday_today?
    birthdate.month == Time.zone.now.month &&
      birthdate.day == Time.zone.now.day
  end

  def pending_vacation_requests?
    vacation_requests.pending.exists?
  end

  def pending_wallet_requests?
    my_way_points_transaction_requests.pending.exists?
  end

  def anniversary_today?
    hire_date.month == Time.zone.now.month &&
      hire_date.day == Time.zone.now.day &&
      ((Time.zone.today - hire_date).to_f / 365) >= 1
  end

  def employee_total_days
    return unless vacation_control

    (worked_months * 0.83).round
  end

  def employee_enjoyed_days
    return unless vacation_control

    enjoyed_days = 0
    vacation_requests.accepted.each do |vacation_request|
      vacation_request.vacations.each do |vacation|
        enjoyed_days += 1 if vacation.date < Time.zone.today
      end
    end
    enjoyed_days
  end

  def approved_days
    return unless vacation_control

    vacation_requests.accepted.joins(:vacations).count
  end

  def employee_available_days
    return unless vacation_control

    (employee_total_days - approved_days) + seniority_days
  end

  def my_way_points_available
    approved_credit_points = my_way_points_transaction_requests.where(request_type: :credit,
                                                                      status: :approved).sum(:request_points)
    debit_points_not_denied = my_way_points_transaction_requests.where(request_type: :debit)
                                                                .where.not(status: :denied).sum(:request_points)

    approved_credit_points - debit_points_not_denied
  end

  # Every 2 years gain 1 seniority day up to a maximum of 5
  def employee_seniority_days
    return unless vacation_control

    seniority_days = ((Time.zone.today - hire_date).to_f / (365 * 2)).floor
    seniority_days = 5 if seniority_days > 5
    seniority_days
  end

  def generate_reset_token
    self.reset_token = SecureRandom.urlsafe_base64
    self.reset_token_created_at = Time.current
    save!
  end

  def reset_token_valid?
    reset_token.present? && reset_token_created_at >= 24.hours.ago
  end

  def clear_reset_token
    update(reset_token: nil, reset_token_created_at: nil)
  end

  private

  def default_schedule
    work_days = %w[Mondays Tuesdays Wednesdays Thursdays Fridays]

    schedule = create_schedule(total_work_hours: 40)
    work_days.each do |wd|
      day = schedule.work_days.create(week_day: wd)
      day.periods.create(start_time: "8:00am", end_time: "12:00pm")
      day.periods.create(start_time: "1:00pm", end_time: "5:00pm")
    end
  end

  def default_notification_setting
    create_notification_setting
  end

  def worked_months
    current_date = Time.zone.today

    years_in_months = (current_date.year - hire_date.year) * 12
    months_diff = current_date.month - hire_date.month
    compare_days = current_date.day >= hire_date.day ? 0 : 1

    years_in_months + months_diff - compare_days
  end

end
