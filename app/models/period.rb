# frozen_string_literal: true

# Class to record periods
class Period < ApplicationRecord

  validates :start_time, :end_time, presence: true

  belongs_to :work_day

end
