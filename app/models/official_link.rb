# frozen_string_literal: true

# Class to record official links
class OfficialLink < ApplicationRecord

  validates :name, :hyperlink, presence: true

end
