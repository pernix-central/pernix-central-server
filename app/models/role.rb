# frozen_string_literal: true

# Class to record roles
class Role < ApplicationRecord

  validates :name, :tier, presence: true

  has_many :employees, dependent: :nullify

end
