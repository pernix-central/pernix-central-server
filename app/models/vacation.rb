# frozen_string_literal: true

# Class to record vacations
class Vacation < ApplicationRecord

  belongs_to :vacation_request

end
