# frozen_string_literal: true

# Class to record comments
class Comment < ApplicationRecord

  validates :text, presence: true

  belongs_to :employee, optional: true
  belongs_to :suggestion

end
