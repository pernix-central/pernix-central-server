# frozen_string_literal: true

# Class to record schedules
class Schedule < ApplicationRecord

  belongs_to :employee
  has_many :work_days, dependent: :destroy

  accepts_nested_attributes_for :work_days, allow_destroy: true

  def update_work_hours
    work_seconds = 0
    work_days.each do |work_day|
      work_day.periods.each do |period|
        work_seconds += (period.end_time.to_i - period.start_time.to_i)
      end
    end
    self.total_work_hours = (work_seconds / 60.0 / 60.0).round(2)

    save
  end

end
