# frozen_string_literal: true

module Checklist

  # Class to record checklist task
  class Task < ApplicationRecord

    belongs_to :employee
    has_many :completions, class_name: "Checklist::Completion", dependent: :destroy

    validates :task_name, :task_type, presence: true

    enum task_type: { global: "global", custom: "custom" }

    scope :by_employee, ->(employee_id) { where(employee_id: employee_id).or(where(task_type: "global")) }

  end

end
