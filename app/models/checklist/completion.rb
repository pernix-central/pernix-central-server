# frozen_string_literal: true

module Checklist

  # Class to record checklist completion
  class Completion < ApplicationRecord

    belongs_to :employee
    belongs_to :task, class_name: "Checklist::Task"

  end

end
