# frozen_string_literal: true

# Class to record employee notification settings
class NotificationSetting < ApplicationRecord

  belongs_to :employee

  validates :anniversary, :birthday, :change_role, :vacation_request_submitted, :vacation_request_reviewed,
            :vacation_request_reminder, :suggestion_responses, :change_schedule, presence: true

end
