# frozen_string_literal: true

# Class to record workdays
class WorkDay < ApplicationRecord

  validates :week_day, presence: true

  belongs_to :schedule
  has_many :periods, dependent: :destroy

  accepts_nested_attributes_for :periods, allow_destroy: true

end
