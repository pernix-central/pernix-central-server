# frozen_string_literal: true

require "faker"

# Class to record suggestions
class Suggestion < ApplicationRecord

  before_save :gen_random_author

  validates :title, :description, presence: true

  belongs_to :employee, dependent: :destroy
  has_many :comments, dependent: :destroy

  private

  def gen_random_author
    self.author = "Anonymus #{Faker::Food.vegetables}"
  end

end
