# frozen_string_literal: true

# Class to record notifications
class Notification < ApplicationRecord

  has_many :notification_employees, dependent: :destroy
  has_many :employees, through: :notification_employees

  validates :content, :notification_type, presence: true

  enum notification_type: { my_anniversary: "my_anniversary", anniversary: "anniversary",
                            my_birthday: "my_birthday", birthday: "birthday",
                            role_change: "role_change", suggestion_responses: "suggestion_responses",
                            vacation_request_revision: "vacation_request_revision",
                            vacation_request_submission: "vacation_request_submission",
                            vacation_request_reminder: "vacation_request_reminder",
                            change_schedule: "change_schedule",
                            admin_vacation_request_revision: "admin_vacation_request_revision",
                            wallet_point_redemption_request_submission: "wallet_point_redemption_request_submission",
                            wallet_point_request_revision: "wallet_point_request_revision",
                            wallet_point_redemption_request_revision: "wallet_point_redemption_request_revision",
                            wallet_request_reminder: "wallet_request_reminder",
                            admin_wallet_request_revision: "admin_wallet_request_revision",
                            request_removal: "request_removal" }

end
