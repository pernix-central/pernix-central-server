# frozen_string_literal: true

# Class to record vacation requests
class VacationRequest < ApplicationRecord

  has_many :vacations, dependent: :destroy
  has_many :notification_employees, as: :notifyable, dependent: :destroy

  belongs_to :employee, class_name: "Employee"
  belongs_to :reviewer, class_name: "Employee", optional: true

  enum status: { accepted: "Accepted", denied: "Denied", pending: "Pending", cancelled: "Cancelled" }

  scope :pending_vacation_requests, lambda {
    VacationRequest.connection.select_all(
      "
        SELECT e.first_name || ' ' || e.last_name AS employee_name,
               COUNT(v.id) AS days_requested,
               DATE(vr.created_at)::VARCHAR AS submission_date,
               DATE_PART('day', CURRENT_DATE - vr.created_at) ||
               CASE
                   WHEN DATE_PART('day', CURRENT_DATE - vr.created_at) = 1 THEN ' day ago'
                   ELSE ' days ago'
               END AS days_since_submission
        FROM vacation_requests vr
        LEFT JOIN employees e ON e.id = vr.employee_id
        LEFT JOIN vacations v ON v.vacation_request_id = vr.id
        WHERE vr.status IN ('Pending')
        GROUP BY employee_name, days_since_submission, submission_date
      ",
    )
  }

  def vacation_dates_output_list
    Vacation.where(vacation_request_id: id)
  end

end
