# frozen_string_literal: true

# Class to record aplication records
class ApplicationRecord < ActiveRecord::Base

  self.abstract_class = true

end
