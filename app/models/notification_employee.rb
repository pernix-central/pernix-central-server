# frozen_string_literal: true

# Class to record notification in employees
class NotificationEmployee < ApplicationRecord

  MAXIMUM_NOTIFICATION_TIMEFRAME = 3.months

  belongs_to :notification
  belongs_to :notifyable, polymorphic: true, optional: true
  belongs_to :employee, class_name: "Employee"
  belongs_to :sender, class_name: "Employee", optional: true

  scope :read, lambda {
    where(read: true)
  }

  scope :expired_read, lambda {
    read.where("updated_at < ?", Date.current - 7)
  }

  scope :expired_due_to_maximum_notification_timeframe, lambda {
    where("updated_at < ?", Date.current - MAXIMUM_NOTIFICATION_TIMEFRAME)
  }

  scope :expired_birthday_types, lambda {
    joins(:notification)
      .where(notifications: {
               notification_type: [
                 Notification.notification_types[:birthday],
                 Notification.notification_types[:my_birthday],
               ],
             })
      .where("notification_employees.updated_at < ?", Date.current - 1.month)
  }

  scope :expired_anniversary_types, lambda {
    joins(:notification)
      .where(notifications: {
               notification_type: [
                 Notification.notification_types[:anniversary],
                 Notification.notification_types[:my_anniversary],
               ],
             })
      .where("notification_employees.updated_at < ?", Date.current - 1.month)
  }

end
