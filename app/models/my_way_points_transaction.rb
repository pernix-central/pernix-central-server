# frozen_string_literal: true

# Class to record my waypoints
class MyWayPointsTransaction < ApplicationRecord

  validates :request_name, :request_points, :request_type, :request_comments, :status, presence: true

  belongs_to :employee, class_name: "Employee"
  belongs_to :reviewer, class_name: "Employee", optional: true
  has_many :notification_employees, as: :notifyable, dependent: :destroy

  enum status: { pending: "Pending", approved: "Approved", denied: "Denied", cancelled: "Cancelled" }
  enum request_type: { debit: "Debit", credit: "Credit" }

  def wallet_request_revision_notification_type
    debit? ? "wallet_point_redemption_request_revision" : "wallet_point_request_revision"
  end

end
