# frozen_string_literal: true

# Employee authentication
class EmployeeAuthentication < ActiveModelSerializers::Model

  attributes :token, :employee

  def initialize(email, password)
    super()
    @email = email
    @password = password
    @employee = nil
    @token = nil
  end

  # Service entry point
  def authenticate
    @employee = authenticated_employee
    @token = generate_token
  end

  def verify_email
    @employee = verify_employee_email
    @token = generate_token
  end

  private

  attr_reader :email, :password

  def generate_token
    JsonWebToken.encode({ employee_id: @employee.id }, 2.months.from_now.to_i)
  end

  def authenticated_employee
    employee = Employee.find_by(email: email)
    return employee if employee&.authenticate(password)

    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end

  def verify_employee_email
    employee = Employee.find_by(email: email)
    return employee if employee

    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end

end
