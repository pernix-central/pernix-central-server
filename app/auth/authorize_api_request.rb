# frozen_string_literal: true

# Authorize requests to the API
class AuthorizeApiRequest

  def initialize(headers = {})
    @headers = headers
  end

  # Service entry point - return valid employee object
  def call
    {
      employee: employee,
    }
  end

  private

  attr_reader :headers

  def employee
    # check if employee is in the database
    # memoize employee object
    @employee ||= Employee.find(decoded_auth_token[:employee_id]) if decoded_auth_token
    # handle employee not found
  rescue ActiveRecord::RecordNotFound => e
    # raise custom error
    raise(
      ExceptionHandler::InvalidToken,
      ("#{Message.invalid_token} #{e.message}"),
    )
  end

  # decode authentication token
  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  # check for token in `Authorization` header
  def http_auth_header
    return headers["Authorization"].split(" ").last if headers["Authorization"].present?

    raise(ExceptionHandler::MissingToken, Message.missing_token)
  end

end
