# frozen_string_literal: true

require "faker"

# Class to serialize the comments
class CommentSerializer < ActiveModel::Serializer

  attributes :id, :text, :author, :created_at, :updated_at

  def author
    if !object.anonymus && object.employee.present?
      "#{object.employee.first_name} #{object.employee.last_name}"
    else
      "Anonymus #{Faker::Games::Pokemon.name}"
    end
  end

end
