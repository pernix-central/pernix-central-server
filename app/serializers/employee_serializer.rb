# frozen_string_literal: true

# Class to serialize the employees
class EmployeeSerializer < ActiveModel::Serializer

  attributes :id, :first_name, :last_name, :birthdate, :email,
             :has_car, :blog_url, :computer_serial_number, :active,
             :mac_address, :hire_date, :on_vacation, :can_be_mentor, :vacation_control,
             :phone_number, :id_number, :approved_days, :employee_available_days,
             :employee_seniority_days, :seniority_days, :employee_total_days,
             :employee_enjoyed_days, :has_pending_vacation_requests, :has_pending_wallet_requests,
             :my_way_points_available, :profile

  has_one :schedule
  belongs_to :role

  def profile
    return unless object.profile.attached?

    if Rails.env.development?
      Rails.application.routes.url_helpers.rails_blob_url(object.profile, host: "localhost", port: 3000)
    else
      object.profile.blob.url
    end
  end

  def has_pending_vacation_requests
    object.pending_vacation_requests? if object.vacation_control
  end

  def has_pending_wallet_requests
    object.pending_wallet_requests?
  end

end
