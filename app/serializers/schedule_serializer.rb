# frozen_string_literal: true

# Class to serialize the schedules
class ScheduleSerializer < ActiveModel::Serializer

  attributes :id, :total_work_hours

  has_many :work_days

  def work_days
    object.work_days.order(:id)
  end

end
