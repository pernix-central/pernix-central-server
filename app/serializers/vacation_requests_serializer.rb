# frozen_string_literal: true

# Class to serialize the vacation requests
class VacationRequestsSerializer < ActiveModel::Serializer

  attributes :id, :requested_date, :client_approval, :status,
             :employee_name, :employee_id, :reviewer_name, :review_date,
             :vacation_dates_output_list

  def employee_name
    object.employee.full_name
  end

  def reviewer_name
    object&.reviewer&.full_name
  end

end
