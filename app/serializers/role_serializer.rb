# frozen_string_literal: true

# Class to serialize the roles
class RoleSerializer < ActiveModel::Serializer

  attributes :id, :name, :tier

end
