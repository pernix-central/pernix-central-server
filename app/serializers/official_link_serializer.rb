# frozen_string_literal: true

# Class to serialize the official links
class OfficialLinkSerializer < ActiveModel::Serializer

  attributes :id, :name, :hyperlink

end
