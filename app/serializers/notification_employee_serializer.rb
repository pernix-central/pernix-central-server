# frozen_string_literal: true

# Class to serialize the notification employee
class NotificationEmployeeSerializer < ActiveModel::Serializer

  attributes :id, :notification_content, :notification_type, :read, :created_at, :sender_id, :extra_information,
             :notifyable_id

  def notification_content
    return role_change_content if object.notification.role_change?
    if object.notification.admin_vacation_request_revision? || object.notification.admin_wallet_request_revision?
      return revised_admin_notification_content
    end
    return removed_request_content if object.notification.request_removal?

    "#{object.notification.content}#{object.sender&.full_name}"
  end

  def role_change_content
    "#{object.notification.content}#{object.extra_information}"
  end

  def revised_admin_notification_content
    request = object.notifyable

    object.notification.content.gsub("$1", request.employee.full_name)
  end

  def removed_request_content
    removed_request_data = object.extra_information.split(",")

    object.notification.content.gsub("$1", removed_request_data[0]).gsub("$2", removed_request_data[1])
  end

  def notification_type
    object.notification.notification_type
  end

end
