# frozen_string_literal: true

# Class to serialize the employee authentications
class EmployeeAuthenticationSerializer < ActiveModel::Serializer

  attribute :token, key: :auth_token

  has_one :employee

end
