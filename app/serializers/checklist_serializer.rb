# frozen_string_literal: true

# Class to serialize the checklist
class ChecklistSerializer < ActiveModel::Serializer

  attributes :id, :task_name, :task_type

end
