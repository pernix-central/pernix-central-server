# frozen_string_literal: true

# Class to serialize the my_way_points_transaction
class MyWayPointsTransactionSerializer < ActiveModel::Serializer

  attributes :id, :request_name, :request_points, :request_type, :request_comments,
             :status, :employee_name, :employee_id, :reviewer_name, :review_date, :created_at,
             :formatted_created_at, :is_marked

  def employee_name
    object.employee.full_name
  end

  def reviewer_name
    object.reviewer&.full_name
  end

  def formatted_created_at
    object.created_at.strftime("%b %d, %Y")
  end

  attribute :is_marked do
    false
  end

end
