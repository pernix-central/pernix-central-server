# frozen_string_literal: true

# Class to serialize the pernix site employees
class PernixSiteEmployeeSerializer < ActiveModel::Serializer

  attributes :id, :name, :role

  def name
    "#{object.first_name} #{object.last_name}"
  end

end
