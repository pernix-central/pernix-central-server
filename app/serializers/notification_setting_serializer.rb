# frozen_string_literal: true

# Class to serialize the notification settings
class NotificationSettingSerializer < ActiveModel::Serializer

  attributes :id, :anniversary, :birthday, :change_role, :suggestion_responses, :vacation_request_reviewed,
             :vacation_request_submitted, :vacation_request_reminder, :wallet_request_reminder, :change_schedule,
             :request_removal, :wallet_request_reviewed

end
