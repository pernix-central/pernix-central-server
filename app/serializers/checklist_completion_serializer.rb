# frozen_string_literal: true

# Class to serialize the checklist completions
class ChecklistCompletionSerializer < ActiveModel::Serializer

  attributes :id, :task_id, :employee_id

end
