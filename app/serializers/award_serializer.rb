# frozen_string_literal: true

# Class to serialize the awards
class AwardSerializer < ActiveModel::Serializer

  attributes :id, :name, :points

end
