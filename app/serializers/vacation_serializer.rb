# frozen_string_literal: true

# Class to serialize the vacations
class VacationSerializer < ActiveModel::Serializer

  attributes :id, :date

end
