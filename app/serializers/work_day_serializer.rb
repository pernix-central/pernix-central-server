# frozen_string_literal: true

# Class to serialize the workdays
class WorkDaySerializer < ActiveModel::Serializer

  attributes :id, :week_day, :wfh

  has_many :periods

  def periods
    object.periods.order(:start_time)
  end

end
