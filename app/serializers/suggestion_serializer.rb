# frozen_string_literal: true

# Class to serialize the suggestions
class SuggestionSerializer < ActiveModel::Serializer

  attributes :id, :title, :description, :author, :management_resolved

  has_many :comments

end
