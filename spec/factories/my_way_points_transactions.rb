# frozen_string_literal: true

FactoryBot.define do
  factory :my_way_points_transaction do
    request_name { Faker::Lorem.word }
    request_comments { Faker::Lorem.sentence }
    request_points { Faker::Number.between(from: 1, to: 20) }
    request_type { MyWayPointsTransaction.request_types.keys.sample }
    status { MyWayPointsTransaction.statuses.keys.sample }

    association :employee, factory: :employee
  end
end
