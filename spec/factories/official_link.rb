# frozen_string_literal: true

FactoryBot.define do
  factory :official_link do
    name { Faker::Name.name }
    hyperlink { Faker::Internet.url }
  end
end
