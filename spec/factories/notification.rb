# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    content { Faker::Lorem.sentence }
    notification_type { Notification.notification_types.values.sample }

    trait :vacation_request_revision do
      notification_type { "vacation_request_revision" }
      content { "Vacation request revised by " }
      active { true }
    end

    trait :suggestion_responses do
      notification_type { Notification.notification_types[:suggestion_responses] }
      content { "Someone commented on your suggestion " }
      active { true }
    end

    trait :vacation_request_reminder do
      notification_type { Notification.notification_types[:vacation_request_reminder] }
      content { "There are vacation requests pending revision " }
      active { true }
    end

    trait :vacation_request_submission do
      notification_type { Notification.notification_types[:vacation_request_submission] }
      content { "Vacation request submitted by " }
      active { true }
    end

    trait :role_change do
      notification_type { Notification.notification_types[:role_change] }
      content { "Your role has been changed to " }
      active { true }
    end

    trait :my_birthday do
      notification_type { Notification.notification_types[:my_birthday] }
      content { "Happy birthday! From all here at Pernix we wish you a great day" }
      active { true }
    end

    trait :my_anniversary do
      notification_type { Notification.notification_types[:my_anniversary] }
      content { "Congratulations on one more year! From all here at Pernix: WE APPRECIATE YOU!" }
      active { true }
    end

    trait :birthday do
      notification_type { Notification.notification_types[:birthday] }
      content { "Today's birthdays" }
      active { true }
    end

    trait :change_schedule do
      notification_type { Notification.notification_types[:change_schedule] }
      content { "Schedule changed by " }
      active { true }
    end

    trait :admin_vacation_request_revision do
      notification_type { Notification.notification_types[:admin_vacation_request_revision] }
      content { "Vacation request for $1 has been reviewed" }
      active { true }
    end

    trait :wallet_point_redemption_request_submission do
      notification_type { Notification.notification_types[:wallet_point_redemption_request_submission] }
      content { "Wallet point redemption request submitted by " }
      active { true }
    end

    trait :wallet_point_request_revision do
      notification_type { "wallet_point_request_revision" }
      content { "Wallet point request revised by " }
      active { true }
    end

    trait :wallet_point_redemption_request_revision do
      notification_type { "wallet_point_redemption_request_revision" }
      content { "Wallet point redemption request revised by " }
      active { true }
    end

    trait :wallet_request_reminder do
      notification_type { "wallet_request_reminder" }
      content { "There are wallet requests pending revision " }
      active { true }
    end

    trait :admin_wallet_request_revision do
      notification_type { "admin_wallet_request_revision" }
      content { "Wallet point request for $1 has been reviewed" }
      active { true }
    end

    trait :request_removal do
      notification_type { "request_removal" }
      content { "$1 has withdrawn their $2 request" }
      active { true }
    end
  end
end
