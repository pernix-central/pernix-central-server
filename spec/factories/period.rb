# frozen_string_literal: true

FactoryBot.define do
  factory :period do
    work_day
    start_time { Faker::Time.forward(23, :morning) }
    end_time { Faker::Time.forward(23, :evening) }
  end
end
