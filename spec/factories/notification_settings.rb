# frozen_string_literal: true

FactoryBot.define do
  factory :notification_setting do
    employee
    anniversary { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
    birthday { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
    change_role { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
    vacation_request_submitted { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
    vacation_request_reviewed { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
    change_schedule { { "email": Faker::Boolean.boolean, "tray": Faker::Boolean.boolean } }
  end
end
