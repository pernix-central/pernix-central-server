# frozen_string_literal: true

FactoryBot.define do
  factory :employee do
    first_name { Faker::Name.name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    id_number { Faker::IDNumber.valid }
    phone_number { Faker::PhoneNumber.cell_phone }
    birthdate { Faker::Date.birthday(min_age: 18, max_age: 65) }
    password { "Pernix123!!" }
    has_car { Faker::Boolean.boolean }
    blog_url { Faker::Internet.url }
    computer_serial_number { Faker::Code.npi }
    mac_address { Faker::Internet.mac_address }
    hire_date { Faker::Date.between(from: 365.days.ago, to: Time.zone.now.prev_month(2)).to_date }
    on_vacation { Faker::Boolean.boolean }
    can_be_mentor { Faker::Boolean.boolean }
    vacation_control { Faker::Boolean.boolean }

    association :role, factory: :role
  end
end
