# frozen_string_literal: true

FactoryBot.define do
  factory :suggestion do
    employee
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
  end
end
