# frozen_string_literal: true

FactoryBot.define do
  factory :vacation_request do
    employee { FactoryBot.create(:employee) }
    status { "Pending" }
    requested_date { Date.current }

    after :build do |request|
      FactoryBot.create(:vacation, vacation_request: request)
    end
  end
end
