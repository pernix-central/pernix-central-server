# frozen_string_literal: true

FactoryBot.define do
  factory :notification_employee do
    employee
    notification
    sender { create(:employee) }

    trait :read do
      read { true }
    end

    trait :anniversary do
      after :build do |notification|
        notification.notification.anniversary!
      end
    end

    trait :birthday do
      after :build do |notification|
        notification.notification.birthday!
      end
    end

    trait :my_anniversary do
      after :build do |notification|
        notification.notification.my_anniversary!
      end
    end

    trait :my_birthday do
      after :build do |notification|
        notification.notification.my_birthday!
      end
    end

    trait :vacation_request_submission do
      after :build do |notification|
        notification.notification.vacation_request_submission!
      end
    end
  end
end
