# frozen_string_literal: true

FactoryBot.define do
  factory :work_day do
    schedule
    wfh { false }
    week_day { Faker::Date.backward(days: 21).strftime("%A") }
  end
end
