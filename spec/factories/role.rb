# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    name { Faker::Name.name }
    tier { rand 0..2 }

    trait :admin do
      name { "Admin" }
      tier { 0 }
    end

    trait :crafter do
      name { "Crafter" }
      tier { 1 }
    end

    trait :apprentice do
      name { "Apprentice" }
      tier { 2 }
    end
  end
end
