# frozen_string_literal: true

FactoryBot.define do
  factory :award do
    name { Faker::Lorem.sentence }
    points { Faker::Number.between(from: 1, to: 20) }
  end
end
