# frozen_string_literal: true

require "rails_helper"

describe "Roles API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET index" do
    it "returns all the roles" do
      get v1_roles_path, headers: headers
      expect(response).to be_successful
    end
  end
end
