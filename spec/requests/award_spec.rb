# frozen_string_literal: true

require "rails_helper"

describe "Award API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET index" do
    it "returns all the awards" do
      get v1_awards_path, headers: headers
      expect(response).to be_successful
    end
  end
end
