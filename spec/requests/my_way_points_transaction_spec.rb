# frozen_string_literal: true

require "rails_helper"

describe "MyWayPointsTransaction API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET user_wallet" do
    it "succeeds" do
      get user_wallet_v1_my_way_points_transactions_path, headers: headers
      expect(response).to be_successful
    end

    it "returns all user requests" do
      create(:my_way_points_transaction, employee: employee)
      create(:my_way_points_transaction, employee: employee)

      get user_wallet_v1_my_way_points_transactions_path(employee_id: employee.id), headers: headers
      expect(JSON.parse(response.body).length).to eq(2)
    end
  end

  describe "GET wallet_summary" do
    it "succeeds" do
      get wallet_summary_v1_my_way_points_transactions_path, headers: headers
      expect(response).to be_successful
    end

    it "returns all pending credit requests" do
      create(:my_way_points_transaction, request_type: :credit, status: :pending)
      create(:my_way_points_transaction, request_type: :credit, status: :pending)

      get wallet_summary_v1_my_way_points_transactions_path, headers: headers
      expect(JSON.parse(response.body).length).to eq(2)
    end
  end

  describe "POST wallet request" do
    let(:my_way_points_transaction_attrs) { attributes_for(:my_way_points_transaction, employee_id: employee.id) }
    let(:params) { { wallet_request: my_way_points_transaction_attrs } }

    it "succeeds" do
      post v1_my_way_points_transactions_path, params: params.to_json, headers: headers
      expect(response).to be_successful
    end

    it "creates the new wallet request" do
      expect do
        post v1_my_way_points_transactions_path, params: params.to_json, headers: headers
      end.to change(MyWayPointsTransaction, :count).by(1)
    end
  end

  describe "PUT wallet request" do
    let(:my_way_points_transaction) { create(:my_way_points_transaction, status: :pending) }
    let(:params) do
      {
        wallet_request: {
          status: "denied",
          reviewer_id: employee.id,
        },
      }
    end

    it "succeeds" do
      put v1_my_way_points_transaction_path(my_way_points_transaction), params: params.to_json, headers: headers
      expect(response).to be_successful
    end

    it "updates my_way_points_transaction" do
      put v1_my_way_points_transaction_path(my_way_points_transaction), params: params.to_json, headers: headers
      expect(my_way_points_transaction.reload).to have_attributes(params[:wallet_request])
    end
  end

  describe "PUT update_through_email" do
    let(:reviewer) { create(:employee) }
    let(:my_way_points_transaction) { create(:my_way_points_transaction, status: :pending) }
    let(:params) do
      {
        id: my_way_points_transaction.id,
        status: "approved",
        reviewer_id: reviewer.id,
      }
    end

    before do
      put update_through_email_v1_my_way_points_transaction_path(my_way_points_transaction),
          params: params.to_json,
          headers: headers
    end

    it "succeeds" do
      expect(response).to be_successful
    end

    it "updates wallet request" do
      expect(my_way_points_transaction.reload.status).to eq("approved")
    end

    it "returns a message indicating the Wallet request successfully approved" do
      expect(JSON.parse(response.body)["message"]).to eq("Wallet request successfully approved")
    end
  end

  describe "PUT update_wallets_status" do
    let(:my_way_points_transactions) do
      create_list(:my_way_points_transaction, 3, status: :pending)
    end

    let(:params) do
      {
        ids: my_way_points_transactions.map(&:id),
        status: "denied",
      }
    end

    before do
      put update_wallets_status_v1_my_way_points_transactions_path,
          params: params.to_json,
          headers: headers
    end

    it "succeeds" do
      expect(response).to be_successful
    end

    it "updates wallet request" do
      my_way_points_transactions.each do |transaction|
        expect(transaction.reload.status).to eq("denied")
      end
    end

    it "returns a message indicating the Wallet request successfully denied" do
      expect(JSON.parse(response.body)["message"]).to eq("Wallet requests successfully #{params[:status]}.")
    end
  end

  describe "DESTROY my way points transaction" do
    let!(:my_way_points_transaction) { create(:my_way_points_transaction) }

    it "succeeds" do
      delete v1_my_way_points_transaction_path(my_way_points_transaction), headers: headers
      expect(response).to be_ok
    end

    it "destroy my way points transaction" do
      expect do
        delete v1_my_way_points_transaction_path(my_way_points_transaction), headers: headers
      end.to change(MyWayPointsTransaction, :count).by(-1)
    end
  end
end
