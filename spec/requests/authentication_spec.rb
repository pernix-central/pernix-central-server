# frozen_string_literal: true

require "rails_helper"

describe "Authentication", type: :request do
  describe "POST /auth/login" do
    let!(:employee) { create(:employee) }
    let(:headers) { valid_headers.except("Authorization") }
    let(:valid_credentials) do
      {
        email: employee.email,
        password: employee.password,
      }.to_json
    end

    let(:invalid_credentials) do
      {
        email: Faker::Internet.email,
        password: Faker::Internet.password,
      }.to_json
    end

    context "when request is valid" do
      before { post v1_auth_login_path, params: valid_credentials, headers: headers }

      it "returns an authentication token" do
        expect(json["auth_token"]).not_to be_nil
      end

      it "returns the employee" do
        expect(json["employee"]).not_to be_nil
      end
    end

    context "when request is invalid" do
      before { post v1_auth_login_path, params: invalid_credentials, headers: headers }

      it "returns a failure message" do
        expect(json["message"]).to match(/Invalid credentials/)
      end
    end
  end
end
