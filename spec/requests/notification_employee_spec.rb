# frozen_string_literal: true

require "rails_helper"

describe "Notification Employee API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET notifications" do
    it "returns all employee notifications" do
      get notifications_messages_v1_notification_employees_path, headers: headers
      expect(response).to be_successful
    end
  end

  describe "PUT notification employee" do
    let(:notification_employee) { create(:notification_employee) }

    it "update the correct notification" do
      put v1_notification_employee_path(notification_employee), headers: headers
      expect(notification_employee.reload.read).to eq(true)
    end
  end

  describe "GET update all" do
    it "update all employee notifications" do
      get update_all_v1_notification_employees_path, headers: headers
      expect(response).to be_successful
    end
  end
end
