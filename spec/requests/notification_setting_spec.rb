# frozen_string_literal: true

require "rails_helper"

describe "NotificationSettings API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET show" do
    it "succeeds" do
      get show_employee_settings_v1_notification_settings_path, headers: headers
      expect(response).to be_successful
    end
  end

  describe "PUT notification_setting" do
    let(:notification_setting) { create(:notification_setting, employee_id: employee.id) }
    let!(:params) do
      {
        notification_setting: {
          anniversary:
          {
            "email": false,
            "tray": false,
          },
        },
      }
    end

    before { put v1_notification_setting_path(notification_setting), params: params.to_json, headers: headers }

    it "succeeds" do
      expect(response).to be_successful
    end

    it "updates notification_setting for anniversary by email" do
      expect(notification_setting.reload.anniversary[:email]).to be_falsy
    end

    it "updates notification_setting for anniversary by tray" do
      expect(notification_setting.reload.anniversary[:tray]).to be_falsy
    end
  end

end
