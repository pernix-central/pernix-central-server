# frozen_string_literal: true

require "rails_helper"

describe "Employees API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  # Employee create test suite
  describe "POST /v1_employees" do
    let(:role) { create(:role) }
    let(:valid_attributes) do
      { employee: attributes_for(:employee, role_id: role.id) }
    end

    context "when valid request" do
      before { post v1_employees_path, params: valid_attributes.to_json, headers: headers }

      it "creates a new employee" do
        expect(response).to have_http_status(:created)
      end

      it "returns success message" do
        expect(json["message"]).to match(/Account created successfully/)
      end

      it "returns an authentication token" do
        expect(json["auth_token"]).not_to be_nil
      end
    end

    context "when email is already taken" do
      let(:existing_employee) { create(:employee, email: "email@example.com") }
      let(:repeated_employee) do
        {
          employee: attributes_for(:employee,
                                   email: existing_employee.email,
                                   password_confirmation: employee.password),
        }
      end

      before { post v1_employees_path, params: repeated_employee.to_json, headers: headers }

      it "does not create the employee" do
        expect(response).to be_unprocessable
      end
    end

    context "when invalid request" do
      let(:invalid_attributes) do
        {
          employee: attributes_for(:employee,
                                   first_name: "",
                                   last_name: "",
                                   password: "",
                                   email: "",
                                   password_confirmation: "",
                                   birthdate: ""),
        }
      end

      before { post v1_employees_path, params: invalid_attributes.to_json, headers: headers }

      it "does not create a new employee" do
        expect(response).to be_unprocessable
      end
    end
  end

  describe "GET index" do
    it "returns all the employees" do
      get v1_employees_path, headers: headers
      expect(response).to be_ok
    end
  end

  describe "GET show" do
    let(:employee) { create(:employee) }

    it "gets the correct employee" do
      get v1_employee_path(employee), headers: headers
    end
  end

  describe "PUT employee" do
    let(:employee) { create(:employee) }
    let(:params) do
      {
        blog_url: "www.xxx.com",
        role: "Apprentice",
      }
    end

    before do
      put v1_employee_path(employee), params: params.to_json, headers: headers
    end

    it "succeeds" do
      expect(response).to be_ok
    end

    it "updates employee" do
      expect(employee.reload.blog_url).to eq "www.xxx.com"
    end
  end

  describe "PUT employee status" do
    let(:other_employee) { create(:employee) }
    let(:params) { { active: true } }

    before do
      put change_status_v1_employee_path(other_employee), params: params.to_json, headers: headers
    end

    it "succeeds" do
      expect(response).to be_ok
    end

    it "updates employee STATUS" do
      expect(other_employee.reload.active).to eq true
    end
  end

  describe "PATCH employee password" do
    let(:other_employee) { create(:employee) }
    let(:params) do
      { employee:
        {
          password: "test",
          password_confirmation: "test",
        } }
    end

    it "succeeds" do
      patch change_password_v1_employee_path(other_employee), params: params.to_json, headers: headers
      expect(response).to be_successful
    end
  end
end
