# frozen_string_literal: true

require "rails_helper"

describe "OfficialLink API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET index" do
    it "returns all the official links" do
      get v1_official_links_path, headers: headers
      expect(response).to be_successful
    end
  end

  describe "GET show" do
    let(:official_link) { create(:official_link) }

    it "gets the correct official link" do
      get v1_official_link_path(official_link), headers: headers
      expect(assigns(:official_link)).to eq(official_link)
    end
  end

  describe "POST official link" do
    let(:official_link_attrs) { attributes_for(:official_link) }
    let(:params) { { official_link: official_link_attrs } }

    it "succeeds" do
      post v1_official_links_path, params: params.to_json, headers: headers
      expect(response).to be_successful
    end

    it "creates the new official link" do
      expect do
        post v1_official_links_path, params: params.to_json, headers: headers
      end.to change(OfficialLink, :count).by(1)
    end
  end

  describe "PUT official link" do
    let(:official_link) { create(:official_link) }
    let(:params) do
      {
        official_link: {
          name: "new name",
          hyperlink: "https://www.example.com/",
        },
      }
    end

    it "succeeds" do
      put v1_official_link_path(official_link), params: params.to_json, headers: headers
      expect(response).to be_successful
    end

    it "updates official link" do
      put v1_official_link_path(official_link), params: params.to_json, headers: headers
      expect(official_link.reload).to have_attributes(params[:official_link])
    end
  end

  describe "DESTROY official link" do
    let!(:official_link) { create(:official_link) }

    it "succeeds" do
      delete v1_official_link_path(official_link), headers: headers
      expect(response).to be_ok
    end

    it "destroy official link" do
      expect do
        delete v1_official_link_path(official_link), headers: headers
      end.to change(OfficialLink, :count).by(-1)
    end
  end
end
