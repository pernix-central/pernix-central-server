# frozen_string_literal: true

require "rails_helper"

describe "Suggestions API" do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET index" do
    it "succeeds" do
      get v1_suggestions_path, headers: headers
      expect(response).to be_successful
    end
  end

  describe "GET show" do
    let(:suggestion) { create(:suggestion) }

    it "succeeds" do
      get v1_suggestions_path(suggestion), headers: headers
      expect(response).to be_successful
    end
  end

  describe "POST suggestion" do
    let(:params) { attributes_for(:suggestion) }

    it "creates a suggestion" do
      post v1_suggestions_path, params: params.to_json, headers: headers
      expect(response).to be_successful
    end
  end

  describe "POST mark as resolved" do
    let(:suggestion) { create(:suggestion) }
    let(:expected) do
      {
        suggestion: {
          management_resolved: true,
        },
      }
    end

    it "succeeds" do
      put v1_suggestion_mark_resolved_path(suggestion), headers: headers
      expect(response).to be_successful
    end

    it "marks the suggestion as resolved" do
      put v1_suggestion_mark_resolved_path(suggestion), headers: headers
      expect(suggestion.reload).to have_attributes(expected[:suggestion])
    end
  end

  describe "PUT mark as unresolved" do
    let(:suggestion) { create(:suggestion) }
    let(:expected) do
      {
        suggestion: {
          management_resolved: false,
        },
      }
    end

    it "succeeds" do
      put v1_suggestion_mark_unresolved_path(suggestion), headers: headers
      expect(response).to be_successful
    end

    it "marks the suggestion as unresolved" do
      put v1_suggestion_mark_unresolved_path(suggestion), headers: headers
      expect(suggestion.reload).to have_attributes(expected[:suggestion])
    end
  end
end
