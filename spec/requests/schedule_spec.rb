# frozen_string_literal: true

require "rails_helper"

describe "Schedules API", type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  describe "GET show schedule" do
    let(:schedule) { create(:schedule, work_days: []) }

    it "gets the correct schedule" do
      get v1_employee_schedule_path(schedule.employee, schedule), headers: headers
      expect(assigns(:schedule)).to eq(schedule)
    end
  end

  describe "PUT update schedule" do
    let(:schedule) { employee.schedule }
    let(:params) do
      {
        schedule: {
          id: schedule.id,
          work_days_attributes: [
            {
              id: schedule.work_days.first.id,
              week_day: "Mondays",
              wfh: true,
            },
          ],
        },
      }
    end

    it "updates the schedule" do
      put v1_employee_schedule_path(employee, schedule), headers: headers,
                                                         params: params.to_json
      expect(schedule.work_days.first.reload).to have_attributes(wfh: true, week_day: "Mondays")
    end
  end

end
