# frozen_string_literal: true

require "rails_helper"

describe "Comments API" do
  let(:employee) { create(:employee) }
  let(:headers) { valid_headers }

  let(:suggestion) { create(:suggestion, employee: employee) }

  describe "POST comment" do
    context "for suggestions" do
      let(:params) { attributes_for(:comment, suggestion_id: suggestion.id) }

      it "creates the comment" do
        post v1_suggestion_comments_path(suggestion), params: params.to_json, headers: headers
        expect(response).to be_successful
      end
    end
  end
end
