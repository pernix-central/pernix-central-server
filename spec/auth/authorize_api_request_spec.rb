# frozen_string_literal: true

require "rails_helper"

# rubocop:disable RSpec/NestedGroups
RSpec.describe AuthorizeApiRequest do
  subject(:invalid_request_obj) { described_class.new({}) }

  let(:request_obj) { described_class.new(header) }

  let(:employee) { create(:employee) }
  let(:header) { { "Authorization" => token_generator(employee.id) } }

  describe "#call" do
    context "when valid request" do
      it "returns employee object" do
        result = request_obj.call
        expect(result[:employee]).to eq(employee)
      end
    end

    context "when invalid request" do
      context "when missing token" do
        it "raises a MissingToken error" do
          expect { invalid_request_obj.call }
            .to raise_error(ExceptionHandler::MissingToken, /Missing token/)
        end
      end

      context "when invalid token" do
        subject(:invalid_request_obj) do
          described_class.new("Authorization" => token_generator(5))
        end

        it "raises an InvalidToken error" do
          expect { invalid_request_obj.call }
            .to raise_error(ExceptionHandler::InvalidToken, /Invalid token/)
        end
      end

      context "when token is expired" do
        subject(:request_obj) { described_class.new(header) }

        let(:header) { { "Authorization" => expired_token_generator(employee.id) } }

        it "raises ExceptionHandler::ExpiredSignature error" do
          expect { request_obj.call }
            .to raise_error(
              ExceptionHandler::InvalidToken,
              /Signature has expired/,
            )
        end
      end

      context "when fake token" do
        subject(:invalid_request_obj) { described_class.new(header) }

        let(:header) { { "Authorization" => "foobar" } }

        it "handles JWT::DecodeError" do
          expect { invalid_request_obj.call }
            .to raise_error(
              ExceptionHandler::InvalidToken,
              /Not enough or too many segments/,
            )
        end
      end
    end
  end
end
# rubocop:enable RSpec/NestedGroups
