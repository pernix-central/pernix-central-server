# frozen_string_literal: true

require "rails_helper"

RSpec.describe EmployeeAuthentication do
  # create test employee
  subject(:invalid_auth_obj) { described_class.new("foo", "bar") }

  let(:valid_auth_obj) { described_class.new(employee.email, employee.password) }

  let(:employee) { create(:employee) }
  # valid request subject

  # invalid request subject

  # Test suite for EmployeeAuthentication#authenticate
  describe "#authenticate" do
    # return token when valid request
    context "when valid credentials" do
      it "assigns the employee" do
        valid_auth_obj.authenticate
        expect(valid_auth_obj.employee).to eq(employee)
      end

      it "assigns an auth token" do
        valid_auth_obj.authenticate
        expect(valid_auth_obj.token).not_to be_nil
      end
    end

    # raise Authentication Error when invalid request
    context "when invalid credentials" do
      it "raises an authentication error" do
        expect { invalid_auth_obj.authenticate }
          .to raise_error(
            ExceptionHandler::AuthenticationError,
            /Invalid credentials/,
          )
      end
    end
  end
end
