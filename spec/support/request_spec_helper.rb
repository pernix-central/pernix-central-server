# frozen_string_literal: true

# Add the module RequestSpecHelper
module RequestSpecHelper

  # Parse JSON response to ruby hash
  def json
    JSON.parse(response.body)
  end

end
