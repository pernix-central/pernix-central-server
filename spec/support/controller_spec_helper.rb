# frozen_string_literal: true

# spec/support/controller_spec_helper.rb
module ControllerSpecHelper

  # generate tokens from employee id
  def token_generator(employee_id)
    JsonWebToken.encode(employee_id: employee_id)
  end

  # generate expired tokens from employee id
  def expired_token_generator(employee_id)
    JsonWebToken.encode({ employee_id: employee_id }, (Time.now.to_i - 10))
  end

  # return valid headers
  def valid_headers
    {
      "Authorization" => token_generator(employee.id),
      "Content-Type" => "application/json",
    }
  end

  # return invalid headers
  def invalid_headers
    {
      "Authorization" => nil,
      "Content-Type" => "application/json",
    }
  end

end
