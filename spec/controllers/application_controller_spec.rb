# frozen_string_literal: true

require "rails_helper"

describe ApplicationController, type: :controller do
  # create test employee
  let!(:employee) { create(:employee) }
  # set headers for authorization
  let(:headers) { { "Authorization" => token_generator(employee.id) } }
  let(:invalid_headers) { { "Authorization" => nil } }

  describe "#authorize_request" do
    context "when auth token is passed" do
      before { allow(request).to receive(:headers).and_return(headers) }

      # private method authorize_request returns current employee
      it "sets the current employee" do
        expect(controller.instance_eval { authorize_request }).to eq(employee)
      end
    end

    context "when auth token is not passed" do
      before do
        allow(request).to receive(:headers).and_return(invalid_headers)
      end

      it "raises MissingToken error" do
        expect { controller.instance_eval { authorize_request } }
          .to raise_error(ExceptionHandler::MissingToken, /Missing token/)
      end
    end
  end
end
