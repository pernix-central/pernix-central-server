# frozen_string_literal: true

require "rails_helper"

RSpec.describe Vacation do
  describe "associations" do
    it { is_expected.to belong_to(:vacation_request) }
  end
end
