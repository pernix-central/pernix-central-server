# frozen_string_literal: true

require "rails_helper"

describe NotificationSetting, type: :model do

  describe "associations" do
    it { is_expected.to belong_to(:employee) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:anniversary) }
    it { is_expected.to validate_presence_of(:birthday) }
    it { is_expected.to validate_presence_of(:change_role) }
    it { is_expected.to validate_presence_of(:vacation_request_submitted) }
    it { is_expected.to validate_presence_of(:vacation_request_reviewed) }
    it { is_expected.to validate_presence_of(:vacation_request_reminder) }
    it { is_expected.to validate_presence_of(:suggestion_responses) }
    it { is_expected.to validate_presence_of(:change_schedule) }
  end
end
