# frozen_string_literal: true

require "rails_helper"

describe WorkDay, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:schedule) }
    it { is_expected.to have_many(:periods) }
  end
end
