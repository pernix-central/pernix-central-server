# frozen_string_literal: true

require "rails_helper"

describe Employee, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone_number) }
    it { is_expected.to validate_presence_of(:birthdate) }
    it { is_expected.to validate_presence_of(:password_digest) }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to validate_uniqueness_of(:id_number) }
  end

  describe "associations" do
    it { is_expected.to have_one(:schedule) }
    it { is_expected.to have_one(:notification_setting) }
    it { is_expected.to belong_to(:role) }
    it { is_expected.to have_many(:tasks) }
    it { is_expected.to have_many(:completions) }
    it { is_expected.to have_many(:suggestions) }
    it { is_expected.to have_many(:notifications).through(:notification_employees) }

    describe "model actions" do
      let(:admin_role) { FactoryBot.create(:role, :admin) }
      let(:crafter_role) { FactoryBot.create(:role, :crafter) }
      let(:admin_employee) do
        FactoryBot.create(
          :employee,
          role: admin_role,
        )
      end
      let(:crafter_employee) do
        FactoryBot.create(
          :employee,
          role: crafter_role,
        )
      end

      it "creates a default schedule after create an employee" do
        expect(admin_employee.schedule.employee_id).to eq(admin_employee.id)
      end

      it "creates the default vacation request reminder notification settings for admins" do
        expect(admin_employee.notification_setting.vacation_request_reminder["notify_on"]).to be_present
      end

      it "creates the default wallet request reminder notification settings for admins" do
        expect(admin_employee.notification_setting.wallet_request_reminder["notify_on"]).to be_present
      end

      it "does not create the default vacation request reminder notification settings for non admins" do
        expect(crafter_employee.notification_setting.vacation_request_reminder["notify_on"]).not_to be_present
      end

      it "does not create the default wallet request reminder notification settings for non admins" do
        expect(crafter_employee.notification_setting.wallet_request_reminder["notify_on"]).not_to be_present
      end

    end

    describe "model metho" do
      let(:employee) { create(:employee) }

      it "calculates available points" do
        create(:my_way_points_transaction, employee: employee, request_type: :credit, status: :approved,
                                           request_points: 10)
        create(:my_way_points_transaction, employee: employee, request_type: :debit, status: :approved,
                                           request_points: 5)
        expect(employee.my_way_points_available).to eq(5)
      end
    end

    describe "model methods" do
      let(:employee) { create(:employee) }

      it "generates a reset token and sets reset_token_created_at" do
        employee.generate_reset_token
        expect(employee.reset_token).to be_present
      end

      it "returns true if reset token is present and created within the last 24 hours" do
        employee.generate_reset_token
        expect(employee).to be_reset_token_valid
      end

      it "returns false if reset token is not present" do
        expect(employee).not_to be_reset_token_valid
      end

      it "returns false if reset token was created more than 24 hours ago" do
        employee.generate_reset_token
        employee.reset_token_created_at = 25.hours.ago
        expect(employee).not_to be_reset_token_valid
      end

      it "clears the reset token and reset_token_created_at" do
        employee.generate_reset_token
        employee.clear_reset_token
        expect(employee.reset_token).to be_nil
      end
    end
  end
end
