# frozen_string_literal: true

require "rails_helper"

describe Schedule, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:employee) }
    it { is_expected.to have_many(:work_days) }
  end

  describe "model actions" do
    let(:period) { create(:period, start_time: "9:00", end_time: "13:00") }

    it "calculates work_hours" do
      period.work_day.schedule.update_work_hours
      expect(period.work_day.schedule.total_work_hours).to eq(4)
    end
  end
end
