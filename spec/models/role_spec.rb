# frozen_string_literal: true

require "rails_helper"

describe Role, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:tier) }
  end

  describe "associations" do
    it { is_expected.to have_many(:employees) }
  end
end
