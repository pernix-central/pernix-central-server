# frozen_string_literal: true

require "rails_helper"

describe Notification, type: :model do
  subject(:notification) { create(:notification, notification_type: :birthday) }

  describe "associations" do
    it { is_expected.to have_many(:employees).through(:notification_employees) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:content) }
    it { is_expected.to validate_presence_of(:notification_type) }
  end
end
