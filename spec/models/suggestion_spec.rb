# frozen_string_literal: true

require "rails_helper"

describe Suggestion, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:title) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:employee) }
    it { is_expected.to have_many(:comments) }
  end
end
