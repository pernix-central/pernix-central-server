# frozen_string_literal: true

require "rails_helper"

describe Checklist::Completion, type: :model do

  describe "associations" do
    it { is_expected.to belong_to(:employee) }
    it { is_expected.to belong_to(:task) }
  end

end
