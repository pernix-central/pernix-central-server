# frozen_string_literal: true

require "rails_helper"

describe Checklist::Task, type: :model do

  describe "associations" do
    it { is_expected.to belong_to(:employee) }
    it { is_expected.to have_many(:completions) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:task_name) }
    it { is_expected.to validate_presence_of(:task_type) }
  end

end
