# frozen_string_literal: true

require "rails_helper"

describe NotificationEmployee, type: :model do

  describe "associations" do
    it { is_expected.to belong_to(:notification) }
    it { is_expected.to belong_to(:employee) }
  end

  describe "retrieves expired read notifications" do
    let!(:read_notification1) do
      FactoryBot.create(
        :notification_employee,
        :read,
        created_at: Date.current,
        updated_at: Date.current,
      )
    end
    let!(:read_notification2) do
      FactoryBot.create(
        :notification_employee,
        :read,
        created_at: Date.current - 8.days,
        updated_at: Date.current - 8.days,
      )
    end
    let!(:unread_notification) do
      FactoryBot.create(
        :notification_employee,
        created_at: Date.current - 8.days,
        updated_at: Date.current - 8.days,
      )
    end

    it "when they have been read and are not 7 days old" do
      expired_read_notifications = described_class.expired_read
      expect(expired_read_notifications).not_to include(read_notification1)
    end

    it "when they have been read and are more than 7 days old" do
      expired_read_notifications = described_class.expired_read
      expect(expired_read_notifications).to include(read_notification2)
    end

    it "when they have not been read" do
      expired_read_notifications = described_class.expired_read
      expect(expired_read_notifications).not_to include(unread_notification)
    end
  end

  describe "retrieves expired birthday notifications" do
    let!(:recent_birthday_notification1) do
      FactoryBot.create(
        :notification_employee,
        :birthday,
        created_at: Date.current,
        updated_at: Date.current,
      )
    end

    let!(:recent_birthday_notification2) do
      FactoryBot.create(
        :notification_employee,
        :my_birthday,
        created_at: Date.current,
        updated_at: Date.current,
      )
    end

    let!(:month_old_birthday_notification1) do
      FactoryBot.create(
        :notification_employee,
        :birthday,
        created_at: Date.current - 1.month - 5.days,
        updated_at: Date.current - 1.month - 5.days,
      )
    end

    let!(:month_old_birthday_notification2) do
      FactoryBot.create(
        :notification_employee,
        :my_birthday,
        created_at: Date.current - 1.month - 5.days,
        updated_at: Date.current - 1.month - 5.days,
      )
    end

    context "for birthday types" do
      it "when notifications are not more than 1 month old" do
        expired_birthday_notifications = described_class.expired_birthday_types

        expect(expired_birthday_notifications).not_to include(recent_birthday_notification1)
      end

      it "when notifications are more than 1 month old" do
        expired_birthday_notifications = described_class.expired_birthday_types

        expect(expired_birthday_notifications).to include(month_old_birthday_notification1)
      end
    end

    context "for my_birthday types" do
      it "when notifications are not more than 1 month old" do
        expired_birthday_notifications = described_class.expired_birthday_types

        expect(expired_birthday_notifications).not_to include(recent_birthday_notification2)
      end

      it "when notifications are more than 1 month old" do
        expired_birthday_notifications = described_class.expired_birthday_types

        expect(expired_birthday_notifications).to include(month_old_birthday_notification2)
      end
    end
  end

  describe "retrieves expired anniversary notifications" do
    let!(:recent_anniversary_notification1) do
      FactoryBot.create(
        :notification_employee,
        :anniversary,
        created_at: Date.current,
        updated_at: Date.current,
      )
    end

    let!(:recent_anniversary_notification2) do
      FactoryBot.create(
        :notification_employee,
        :my_anniversary,
        created_at: Date.current,
        updated_at: Date.current,
      )
    end

    let!(:month_old_anniversary_notification1) do
      FactoryBot.create(
        :notification_employee,
        :anniversary,
        created_at: Date.current - 1.month - 5.days,
        updated_at: Date.current - 1.month - 5.days,
      )
    end

    let!(:month_old_anniversary_notification2) do
      FactoryBot.create(
        :notification_employee,
        :my_anniversary,
        created_at: Date.current - 1.month - 5.days,
        updated_at: Date.current - 1.month - 5.days,
      )
    end

    context "for anniversary types" do
      it "when notifications are not more than 1 month old" do
        expired_anniversary_notifications = described_class.expired_anniversary_types

        expect(expired_anniversary_notifications).not_to include(recent_anniversary_notification1)
      end

      it "when notifications are more than 1 month old" do
        expired_anniversary_notifications = described_class.expired_anniversary_types

        expect(expired_anniversary_notifications).to include(month_old_anniversary_notification1)
      end
    end

    context "for my_anniversary types" do
      it "when notifications are not more than 1 month old" do
        expired_anniversary_notifications = described_class.expired_anniversary_types

        expect(expired_anniversary_notifications).not_to include(recent_anniversary_notification2)
      end

      it "when notifications are more than 1 month old" do
        expired_anniversary_notifications = described_class.expired_anniversary_types

        expect(expired_anniversary_notifications).to include(month_old_anniversary_notification2)
      end
    end
  end

  describe "retrieves expired notifications in general" do
    before do
      timeframe = described_class::MAXIMUM_NOTIFICATION_TIMEFRAME

      Notification.notification_types.each_value do |value|
        notification = Notification.create(notification_type: value, content: "Test content for #{value}",
                                           active: true)
        FactoryBot.create(
          :notification_employee,
          notification_id: notification.id,
          employee: FactoryBot.create(:employee),
          created_at: Date.current - (timeframe - 1.day),
          updated_at: Date.current - (timeframe - 1.day),
        )
      end
    end

    it "if notifications newer than the maximum notification timeframe" do
      expect(described_class.expired_due_to_maximum_notification_timeframe).to be_empty
    end

    it "if notifications are older than the maximum notification timeframe" do
      timeframe = described_class::MAXIMUM_NOTIFICATION_TIMEFRAME
      described_class.all.find_each do |notification|
        notification.update!(updated_at: Date.current - timeframe - 1.day)
      end
      expect(described_class.expired_due_to_maximum_notification_timeframe.uniq.count).to eq(described_class.count)
    end
  end
end
