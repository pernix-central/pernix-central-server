# frozen_string_literal: true

require "rails_helper"

describe Period, type: :model do
  describe "associations" do
    it { is_expected.to belong_to(:work_day) }
  end
end
